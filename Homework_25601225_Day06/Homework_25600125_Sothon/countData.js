let fs = require('fs');
let writeData = require('./writeData');

function countData(data, condition) {
    return new Promise(function(resolve, reject) {
        output = [];
        group = {};
        if (condition === '') {
            reject('err');
        } else {
            data.map(element => {
                group['_id'] = element['_id'];
                group['friendCount'] = element['friends'].length;
                group['tagsCount'] = element.tags.length
                group['balance'] = element.balance
                output.push(group);
                group = {};
            });

            writeData.writeData('./homework5-1_'+condition+'.json', output);

            resolve(output);         
        }
    });
}

exports.countData = countData;
