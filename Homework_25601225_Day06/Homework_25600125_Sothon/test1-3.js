const fs = require('fs')
let writeData = require('./writeData');

function eyesColorCount(data, gender, color) {
    return new Promise (function (resolve , reject) {
        let sum = 0
        if (data === '' || gender ==='' || color === '') 
            reject('err');
        else {
            for(let key in data) {
                if(key === gender) {
                    for(let value in data[key]) {
                        if(value === color) {
                            sum = data[key][value]
                        }
                    }
                }
            }
        }
        resolve(sum)
    })
}

exports.getEyesColorCount = eyesColorCount