const fs = require('fs')
let writeData = require('./writeData');

function filterGender(data, condition) {
    return new Promise(function (resolve, reject) {
        groupMale = {}
        groupFemale = {}
        group = {}
        if (condition === '') reject(err)
        else {
            let result = data.map(element => {
                Object.keys(element)
                    .map(key => {
                        if (condition === key) {
                            if (element.gender === 'male') {
                                if (groupMale.hasOwnProperty(element[key])) {
                                    groupMale[element[key]]++;
                                } else {
                                    groupMale[element[key]] = 1;
                                }
                            } else if (element.gender === 'female') {
                                if (groupFemale.hasOwnProperty(element[key])) {
                                    groupFemale[element[key]]++;
                                } else {
                                    groupFemale[element[key]] = 1;
                                }
                            }
                        }
                    })
            })
            group['male'] = groupMale
            group['female'] = groupFemale
            writeData.writeData('./homework5-1_eyes.json', group);
            resolve(group);
        }
    })
}

exports.filterGender = filterGender