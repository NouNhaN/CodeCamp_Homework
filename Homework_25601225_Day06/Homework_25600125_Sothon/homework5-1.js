let assert = require('assert');
let fs = require('fs');

let dataEyes = {};
let dataGender = {};
let dataFriends = [];

describe('tdd lab', function() {
    describe('#checkFileExist()', function() {
        it('should have homework1-4.json existed', function(done) {
            fs.access('./homework1-4.json', fs.constants.R_OK | fs.constants.W_OK, (err) => {
                if (err) {
                    done(err);
                } else {
                    done();
                }
            })
        });
        it('should have homework5-1_eyes.json existed', function(done) {
            fs.access('./homework5-1_eyes.json', fs.constants.R_OK | fs.constants.W_OK, (err) => {
                if (err) {
                    done(err);
                } else {
                    done();
                }
            })
        });
        it('should have homework5-1_gender.json existed', function(done) {
            fs.access('./homework5-1_gender.json', fs.constants.R_OK | fs.constants.W_OK, (err) => {
                if (err) {
                    done(err);
                } else {
                    done();
                }
            })
        });
        it('should have homework5-1_friends.json existed', function(done) {
            fs.access('./homework5-1_friends.json', fs.constants.R_OK | fs.constants.W_OK, (err) => {
                if (err) {
                    done(err);
                } else {
                    done();
                }
            })
        });
    });

    describe('#objectKey()', function() {
        it('should have same object key stucture as homework5-1_eyes.json', function(done) {
            let dataExpect = {"male":{"brown":1,"blue":1,"green":1},"female":{"brown":1,"blue":1,"green":1}};            
            let data = fs.readFile('./homework5-1_eyes.json', 'utf8', function(err, data) {       
                if (err) 
                    done(err);
                else {
                    data = JSON.parse(data)
                    assert.deepEqual(Object.keys(dataExpect), Object.keys(data), 'should be Equal')
                    done();
                }                                    
            });
        });
        it('should have same object key stucture as homework5-1_gender.json', function(done) {
            let dataExpect = {'female':1,'male':1};
            let data = fs.readFile('./homework5-1_gender.json', 'utf8', function(err, data) {
                if (err) 
                    done(err);
                else {
                    data = JSON.parse(data)
                    assert.deepEqual(Object.keys(dataExpect), Object.keys(data), 'should be Equal')
                    done();
                }                                    
            });
        });
        it('should have same object key stucture as homework5-1_friends.json', function(done) {
            let dataExpect = {'_id':'5a3711070776d02ed87d2100','friendCount':2, 'tagsCount': 1, 'balance' : 0};
            let data = fs.readFile('./homework5-1_friends.json', 'utf8', function(err, data) {
                if (err) 
                    done(err);
                else {
                    data = JSON.parse(data);
                    assert.deepEqual(Object.keys(data[0]), Object.keys(dataExpect), 'should have same object key stucture as homework5-1_friends.json');
                    done();
                }                                    
            });
            
        });
    });
    describe('#userFriendCount()', function() {
        it('should have size of array input as 23', function(done) {
            assert.equal(dataFriends.length, 23, 'should have size of array input as 23');
            done();
        });
    });
    describe('#sumOfEyes()', function() {
        it('should have sum of eyes as 23', function(done) {
            let eyesAmount = Object.keys(dataEyes).map(function(k) { return dataEyes[k] })
            // .map(function(i) { return dataEyes[k][i] });
            // eyesAmount = eyesAmount.reduce((sum, amount) => {
            //     return sum + amount;
            // }, 0);

            let sum = 0;
            for(let i in eyesAmount) {
                for(let j in eyesAmount[i]) {
                    sum += parseInt(eyesAmount[i][j])
                }
            }
            assert.equal(sum, 23, 'should have sum of eyes as 23');
            done();
        });
    });
    describe('#sumOfGender()', function() {
        it('should have sum of gender as 23', function(done) {
            let genderAmount = Object.keys(dataGender).map(function(k) { return dataGender[k] });
            genderAmount = genderAmount.reduce((sum, amount) => {
                return sum + amount;
            }, 0);
            assert.equal(genderAmount, 23, 'should have sum of gender as 23');    
            done();        
        });
    });
});


/**********************************/
let readData  = require('./readData');
let groupData = require('./groupData');
let countData = require('./countData');
let filterGender = require('./test.js');
let getEyesColorCount = require('./test1-3.js')

async function start() {

    const data = await readData.readData('homework1-4.json');
    employees  = JSON.parse(data);

    // dataEyes    = await groupData.groupData(employees, 'eyeColor');
    dataGender  = await groupData.groupData(employees, 'gender');
    

    dataFriends = await countData.countData(employees, 'friends');
    dataEyes    = await filterGender.filterGender(employees, 'eyeColor');
    eyesColorCount = await getEyesColorCount.getEyesColorCount(dataEyes,'male', 'blue')

    // genderEyeColor = await groupData.groupData(employees, 'gender').groupData(employees, 'eyeColor')
    // console.log(dataEyes);
    // console.log(dataGender);
    // console.log(dataFriends);
}

start();