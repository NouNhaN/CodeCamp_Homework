let fs = require('fs');

function writeData(filename, data) {
    return new Promise(function(resolve, reject) {
        fs.writeFile(filename, JSON.stringify(data), 'utf8', function(err) {
            if (err) 
                reject(err);
            else             
                resolve();
        });
    });
}

exports.writeData = writeData;

