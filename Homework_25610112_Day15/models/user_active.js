class User {
    constructor (db, row) {
        this._db = db;
        if (row) {
            this.id = row.id
            this.firstName = row.first_name
            this.lastName = row.last_name
        };
    };
  
    async save () {
        if (!this.id) {
            const result = await this._db.execute(
                `INSERT INTO users 
                ( first_name, last_name ) 
                values ( ?, ? )
                `, [this.firstName, this.lastName]
            );
    
            this.id = result[0].insertId;
            return;
        };
  
        return this._db.execute(
            `UPDATE users 
            SET first_name = ?,
                last_name = ?
            WHERE id = ?
            `, [this.firstName, this.lastName, this.id]
        );
    };

    async remove () {
        if (this.id) {
            return this._db.execute(
                `DELETE FROM users
                WHERE id = ?
                `, [this.id]
            );
        }
    };
};
  
module.exports = function (db) {
    return {
        async find (id) {
            const [rows] = await db.execute(
                `SELECT
                    id, first_name, last_name
                FROM users
                WHERE id = ?
                `, [id]
            );
            return new User(db, rows[0]);
        },
        async findAll () {
            const [rows] = await db.execute(
                `SELECT
                    id, first_name, last_name
                FROM users
                ORDER BY id`
            );
            return rows.map((row) => new User(db, row))
        },
        async findByUsername (username) {
            const [rows] = await db.execute(
                `SELECT
                    id, first_name, last_name
                FROM users
                WHERE first_name = ?
                `, [username]
            );
            return new User(db, rows[0]);
        }
    }
}