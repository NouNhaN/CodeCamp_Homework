const mysql2 = require('mysql2/promise');
const createUserModel = require('./models/user_active');

async function main () {
    const db = await mysql2.createConnection({
        host: 'localhost',
        user: 'root',
        database: 'codecamp'
    });

    const User = createUserModel(db);

    const user0 = await User.findAll();
    console.log(user0);
    
    const user1 = await User.find(1);
    user1.firstName = 'tester';
    await user1.save();
    console.log(user1);

    const user2 = await User.find(2);
    await user2.remove();
    console.log(user2);

    const user3 = await User.findAll();
    console.log(user3);
    
    const user4 = await User.findByUsername('tester');
    console.log(user4);
};

main();