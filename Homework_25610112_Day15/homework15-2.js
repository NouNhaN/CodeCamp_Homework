const mysql2 = require('mysql2/promise');
const User = require('./repository/user_repo');

async function main () {
    const db = await mysql2.createConnection({
        host: 'localhost',
        user: 'root',
        database: 'codecamp'
    });

    const user0 = await User.findAll(db);
    console.log(user0);

    const user1 = await User.find(db, 1);
    user1.firstName = 'tester';
    await User.save(db, user1);
    console.log(user1);

    const user2 = await User.find(db, 2);
    if (user2) {
        await User.remove(db, user2.id);
    }
    console.log(user2);
    
    const user3 = await User.findAll(db);
    console.log(user3);

    const user4 = await User.findByUsername(db, 'tester');
    console.log(user4);
};

main();