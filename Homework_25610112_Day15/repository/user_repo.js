module.exports = {
    find,
    findAll,
    findByUsername,
    save,
    remove
};

function createEntity (row) {
    if (row) {
        return {
            id: row.id,
            firstName: row.first_name,
            lastName: row.last_name
        };
    };
};
  
async function find (db, id) {
    const [rows] = await db.execute(
        `SELECT
            id, first_name, last_name
        FROM users
        WHERE id = ?
        `, [id]
    );
    return createEntity(rows[0]);
};
  
async function findAll (db) {
    const [rows] = await db.execute(
        `SELECT
            id, first_name, last_name
        FROM users
        ORDER BY id`
    );
    return rows.map(createEntity);
};
  
async function findByUsername (db, username) {
    const [rows] = await db.execute(
        `SELECT
            id, first_name, last_name
        FROM users
        WHERE first_name = ?
        `, [username]
    );
    return createEntity(rows[0]);
};
  
async function save (db, user) {
    if (!user.id) {
        const result = await db.execute(
            `INSERT INTO users (
                first_name, last_name
            ) values (
                ?, ?
            )
            `, [user.firstName, user.lastName]
        );
        user.id = result.insertId;
        return
    };
    return await db.execute(
        `UPDATE users
        SET 
            first_name = ?,
            last_name = ?
        WHERE id = ?
        `, [user.firstName, user.lastName, user.id]
    );
};
  
async function remove (db, id) {
    if (id) {
        return await db.execute(
            `DELETE FROM users
            WHERE id = ?
            `, [id]
        );
    };
};
  
  