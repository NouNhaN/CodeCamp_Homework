"user Strict";
/*
CREATE TABLE `session` (
  `session_key` varchar(255) NOT NULL,
  `session_sess` varchar(255) NOT NULL,
  `session_maxage` int(11) DEFAULT 0,
  PRIMARY KEY (`session_key`)
);
*/
const Koa = require('koa');
const session = require('koa-session');
const mysql2 = require('mysql2/promise');

const dbConfig = require('./config/database');
const pool = mysql2.createPool(dbConfig);

const sessionStore = [];

const sessionConfig = {
    key: 'sess',
    maxAge: 3600 * 1000,
    httpOnly: true,
    store: {
        async get (key, maxAge, { rolling }) {
            sessionStore[key];

            try {
                const [rows] = await pool.execute(
                    `SELECT session_key, session_sess, session_maxage
                    FROM session
                    WHERE session_key = ?
                    `, [key]
                );
                if (!rows[0]) {
                    return;
                } else {
                    return JSON.parse(rows[0].session_sess);
                };
            } catch(err) {
                console.error(err);
            };            
        },
        async set (key, sess, maxAge, { rolling }) {
            sessionStore[key] = sess;

            try {
                await pool.execute(
                    `INSERT INTO session (
                        session_key, session_sess, session_maxage
                    ) VALUE (
                        ?, ?, ?
                    ) ON DUPLICATE KEY UPDATE 
                        session_key = ?,
                        session_sess = ?,
                        session_maxage = ?
                    `, [key, JSON.stringify(sess), maxAge, key, JSON.stringify(sess), maxAge]
                );
            } catch (err) {
                console.error(err); 
            };
        },
        async destroy (key) {
            this.session = null;

            try {
                await pool.execute(
                    `DELETE FROM session
                    WHERE session_key = ?
                    `, [key]
                );
            } catch (err) {
                console.error(err);
            }
        }
    }
};

const app = new Koa();

app.keys = ['supersecret'];
app
    .use(session(sessionConfig, app))
    .use(middleware_session)
    .listen(3000);

function middleware_session (ctx) {
    if (ctx.path === '/favicon.ico') return;

    let n = ctx.session.views || 0;
    ctx.session.views = ++n;
    ctx.body = `${n} views`;
};