"use Strict";

const Koa = require("koa");
const session = require("koa-session");
const render = require("koa-ejs");
const path = require("path");
const bodyParser = require("koa-bodyparser");

const appKoa = new Koa();

// const baseUrl = "http://localhost:3000/";
const portlisten = "3000";

// render(appKoa, {
//   root: path.join(__dirname, "view"),
//   layout: "template",
//   viewExt: "ejs",
//   cache: false,
//   debug: true
// });

appKoa.keys = ["superSecretAppKeys"];
appKoa.use(bodyParser());

const sessionStore = {};
const CONFIG = {
  key: "koa:sess",
  maxAge: 60 * 60 * 1000,
  overwrite: true,
  httpOnly: true,
  store: {
    get(key, maxAge, { rolling }) {
      return sessionStore[key];
    },
    set(key, sess, maxAge, { rolling, changed }) {
      sessionStore[key] = sess;
    },
    destroy(key) {}
  }
};

appKoa.use(session(CONFIG, appKoa));
appKoa.use (async (ctx, next) => {
  if (ctx.method === 'POST' && (ctx.path === '/auth/signin') || (ctx.path === '/auth/signup'))  
  {
      await next();
      return;
  }

  if (!ctx.session || (ctx.session && !ctx.session.userId )) {
    ctx.status = 401;
    ctx.body = {"error": "unauthorized"};
  } else {
    await next();

    // let userRow = await userModel.getUserInfoByUserId(ctx.session.userId);
      // if (userRow.role != 'admin' && ctx.path == '/admin')
      //     ctx.body = "Forbidden!!";
      // else
      //     await next();
  }
});  

const requestLogger = require("./libs/requestLogger");
const errorRecovery = require("./libs/errorRecovery");
const router = require("./routes/twitterRoute");

appKoa.use(requestLogger.logger);
appKoa.use(errorRecovery.error);
appKoa.use(router.routes());
appKoa.use(router.allowedMethods());
appKoa.listen(portlisten);


// const makeAuthCtrl = require("./controllers/auth");
// const authRepo = require("./repositorys/auth");
// const authCtrl = makeAuthCtrl(pool, authRepo);

// const makeUploadCtrl = require("./controllers/upload");
// const uploadRepo = require("./repositorys/upload");
// const uploadCtrl = makeUploadCtrl(pool, uploadRepo);

// const makeUserCtrl = require("./controllers/user");
// const userRepo = require("./repositorys/user");
// const userCtrl = makeUserCtrl(pool, userRepo);

// const maketweetCtrl = require("./controllers/tweet");
// const tweetRepo = require("./repositorys/tweet");
// const tweetCtrl = maketweetCtrl(pool, tweetRepo);

// const makeNotificationCtrl = require("./controllers/notification");
// const notificationRepo = require("./repositorys/notification");
// const notificationCtrl = makeNotificationCtrl(pool, notificationRepo);

// const makeMessageCtrl = require("./controllers/message");
// const messageRepo = require("./repositorys/message");
// const messageCtrl = makeMessageCtrl(pool, messageRepo);
