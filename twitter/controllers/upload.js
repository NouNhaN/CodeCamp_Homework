const multer = require("koa-multer");
const jimp = require("jimp"); //ใช้แล้วจะช้า
//const sharp = require('sharp');
/*
  การ Install sharp
  - https://github.com/felixrieseberg/windows-build-tools
    npm install --global --production windows-build-tools    -> from an elevated PowerShell or CMD.exe (run as Administrator)
*/

const fs = require("fs");

module.exports = function(pool, repository) {
  return {
    async upload(ctx) {
      const upload = multer({ dest: "upload/" });
      await upload.single("file")(ctx);

      const tempFile = ctx.req.file.path;
      const outFile = tempFile + ".jpg";
      await jimp.read(tempFile).then(jimpFile => {
        jimpFile.resize(100, 100).write(outFile);

        ctx.status = 200;
        fs.unlink(tempFile, () => {});
        ctx.body = {};
      });
    }
  };
};
