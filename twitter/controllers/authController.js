"use Strict";

const _ = require("lodash");

module.exports = function(pool, repository) {
  return {
    async signup(ctx) {
      const auth = ctx.request.body;

      if (!_.isEmpty(auth)) {
        (await repository.create(pool, auth)) || "";
        ctx.body = {};
      }
    },
    async signin(ctx) {
      const auth = ctx.request.body;
      if (!_.isEmpty(auth)) {
        const result = await repository.find(pool, auth);
        if (_.isEmpty(result)) {
          ctx.body = { error: "wrong password" };
          ctx.status = 401;
        } else {
          ctx.body = {};
          ctx.session.userId = result.id;
        }
      }
    },
    async signout(ctx) {
      ctx.body = {};
      ctx.status = 200;
      ctx.session = null;
    },
    async verify(ctx) {}
  };
};