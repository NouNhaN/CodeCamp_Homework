"use strict";

const Router = require("koa-router");
const mysql2 = require("mysql2/promise");

const dbConfig = require("../config/database");
const pool = mysql2.createPool(dbConfig);

const authRepo = require("../repositorys/auth");
const auth = require("../controllers/authController")(pool,authRepo);

const router = new Router();

router
  .get("/", async (ctx) => {
    ctx.body = "homepage";
  })
  /*  Auth */
  .post("/auth/signup", auth.signup)
  .post("/auth/signin", auth.signin)
  .get("/auth/signout", auth.signout)
  // .post("/auth/verify", authCtrl.verify)
  /*  Upload */
  // .post("/upload", uploadCtrl.upload)
  /*  User */
  // .patch("/user/:id", userCtrl.update)
  // .put("/user/:id/follow", userCtrl.follow)
  // .delete("/user/:id/follow", userCtrl.unfollow)
  // .get("/user/:id/follow", userCtrl.getfollow)
  // .get("/user/:id/followed", userCtrl.getfollowed)
  /*  Tweet */
  // .get("/tweet", tweetCtrl.list)
  // .post("/tweet", tweetCtrl.create)
  // .put("/tweet/:id/like", tweetCtrl.like)
  // .delete("/tweet/:id/like", tweetCtrl.dislike)
  // .post("/tweet/:id/retweet", tweetCtrl.retweet)
  // .put("/tweet/:id/vote/:voteId", tweetCtrl.vote)
  // .post("/tweet/:id/reply", tweetCtrl.reply)
  /*  Notification */
  // .get("/notification", notificationCtrl.list)
  /*  Direct Message */
  // .get("/message", messageCtrl.list)
  // .get("/message/:userId", messageCtrl.get)
  // .post("/message/:userId", messageCtrl.create);

module.exports = router;