module.exports = {
    create,
    addPhoto,
    like,
    unlike,
    hashTag,
    addTag,
    retweet
};

async function create (db, userId, tweet) {
    const result = await db.execute(
        `INSERT INTO tweets (
            user_id, content, type
        ) values (
            ?, ?, ?
        )`, 
        [userId, tweet.content, tweet.type]
    );
    return result[0].insertId;
};

async function addPhoto (db, tweetId, photoUrl) {
    const result = await db.execute(
        `INSERT INTO tweet_photos (
            tweet_id, url
        ) values (
            ?, ?
        )`, 
        [tweetId, photoUrl]
    );
    return result[0].insertId;
};

async function like (db, userId, tweetId) {
    await db.execute(
        `INSERT INTO tweet_likes (
            user_id, tweet_id
        ) values (
            ?, ?
        )`, 
        [userId, tweetId]
    );
};

async function unlike (db, userId, tweetId) {
    await db.execute(
        `DELETE FROM tweet_likes
        WHERE user_id = ? and tweet_id = ?`, 
        [userId, tweetId]
    );
};

async function hashTag (db, tweetId, hashtag) {
    await db.execute(
        `INSERT INTO tweet_hashtags (
            tweet_id, hashtag
        ) values (
            ?, ?
        )`, 
        [tweetId, hashtag]
    );
};

async function addTag (db, hashtag) {
    await db.execute(
        `INSERT INTO hashtags (
            name
        ) values (
            ?
        )`, 
        [hashtag]
    );
};

async function retweet (db, userId, tweetId, content) {
    await db.execute(
        `INSERT INTO retweets (
            user_id, tweet_id, content
        ) values (
            ?, ?, ?
        )`, 
        [userId, tweetId, content]
    );
};

async function replie (db, tweetId, userId, content) {
    await db.execute(
        `INSERT INTO tweet_replies (
            tweetId, userId, content
        ) values (
            ?, ?, ?
        )`, 
        [tweetId, userId, content]
    );
};