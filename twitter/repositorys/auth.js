module.exports = {
    create,
    find
};

async function create (db, user) {
    const result = await db.execute(
        `INSERT INTO users (
            username, email, password, name, status
        ) VALUES (
            ?, ?, ?, ?, ?
        )`, 
        [user.username, user.email, user.password, user.name, 0]
    );
    return result[0].insertId;
};

async function find (db, user) {
    const [rows] = await db.execute(
        `SELECT
            id, email, password
        FROM users
        WHERE email = ? AND password = ?
        `, [user.email, user.password]
    );
    return rows[0];
};

