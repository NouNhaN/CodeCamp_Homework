module.exports = {
    create,
    markAsRead
}

async function create (db, userId, noti) {
    const result = await db.execute(
        `INSERT INTO notifications (
            user_id, title, content, photo
        ) values (
            ?, ?, ?, ?
        )`, 
        [userId, noti.title, noti.content, noti.photo]
    );
    return result[0].insertId;
};

async function markAsRead (db, notiId) {
    await db.execute(
        `UPDATE notifications
        SET
            is_read = true
        WHERE id = ?`, 
        [notiId]
    );
};