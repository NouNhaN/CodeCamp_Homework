module.exports = {
    create,
    changePhoto,
    changeCover,
    changeTheme,
    changeProfile,
    follow,
    unfollow
};

async function create (db, user) {
    const result = await db.execute(
        `INSERT INTO users (
            username, email, password, name, status
        ) VALUES (
            ?, ?, ?, ?, ?
        )`, 
        [user.username, user.email, user.password, user.name, 0]
    );
    return result[0].insertId;
};

async function changePhoto (db, userId, photoUrl) {
    await db.execute(
        `UPDATE users
        SET photo = ?
        WHERE id = ?`, 
        [photoUrl, userId]
    );
};

async function changeCover (db, userId, photoUrl) {
    await db.execute(
        `UPDATE users
        SET cover = ?
        WHERE id = ?`, 
        [photoUrl, userId]
    );
};

async function changeTheme (db, userId, theme) {
    await db.execute(
        `UPDATE users
        SET theme = ?
        WHERE id = ?`, 
        [theme, userId]
    );
};

async function changeProfile (db, userId, userProf) {
    await db.execute(
        `UPDATE users
        SET location = ?,
            bio = ?,
            birth_date_d = ?,
            birth_date_m = ?,
            birth_date_y = ?,
            show_dm = ?,
            show_y = ?
        WHERE id = ?`, 
        [userProf.location, userProf.bio, 
            userProf.birth_date_d, userProf.birth_date_m, 
            userProf.birth_date_y, userProf.show_dm, userProf.show_y, 
            userId
        ]
    );
};

async function follow (db, followerId, followingId) {
    await db.execute(
        `INSERT INTO follows (
            follower_id, following_id
        ) VALUES (
            ?, ?
        )`, 
        [followerId, followingId]
    );
};

async function unfollow (db, followerId, followingId) {
    await db.execute(
        `DELETE FROM follows
        WHERE follower_id = ? AND following_id = ?`, 
        [followerId, followingId]
    );
};

async function chat (db, senderId, receiverId, content, type) {
    await db.execute(
        `INSERT INTO user_chats (
            senderId, receiverId, content, type
        ) VALUES (
            ?, ?, ?, ?
        )`, 
        [senderId, receiverId, content, type]
    );
};
