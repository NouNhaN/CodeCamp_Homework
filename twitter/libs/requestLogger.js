"use strict";

exports.logger = async (ctx, next) => {
  const start = process.hrtime();
  await next();
  const diff = process.hrtime(start);
  console.log(
    `${ctx.method} - ${ctx.status} - ${ctx.path} - ${ctx.type} - ( ${diff[0] *
      1e9 +
      diff[1]}ns )`
  );
};
