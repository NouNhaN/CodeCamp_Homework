"use strict";

exports.error = async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    console.log("Server Error: " + err);
    ctx.status = 500;
    ctx.body = "Error : 500";
  }
}