const fs = require('fs');

function read_dataFile(Filename) {
    return new Promise(function(resolve, reject) {
        fs.readFile(Filename, 'utf8', function(err, dataFile) {
            if (err)
                reject(err);
            else
                resolve(dataFile);
        });
    });
}

async function create_Data() {
    try {
        let JSON_Data = await read_dataFile('homework3-4.json');
        let employeeData = JSON.parse(JSON_Data);

        let employeeDataNew = [];

        employeeData.forEach(function(currentValue_employeeData, index_employeeData, arr_employeeData){
            let employeeRecordKey = Object.keys(currentValue_employeeData);

            let employeeRecord = {};
            employeeRecordKey.forEach(function(currentValue_employeeRecord, index_employeeRecord, arr_employeeRecord){
                function setData_employeeRecord(Key,Value) {
                    if (currentValue_employeeRecord == Key) {
                        employeeRecord[currentValue_employeeRecord] = Value;
                    }
                }

                setData_employeeRecord('name', arr_employeeData[index_employeeData].name);
                setData_employeeRecord('gender', arr_employeeData[index_employeeData].gender);
                setData_employeeRecord('company', arr_employeeData[index_employeeData].company);
                setData_employeeRecord('email', arr_employeeData[index_employeeData].email);
                setData_employeeRecord('friends', arr_employeeData[index_employeeData].friends);
            });
            employeeDataNew.push(employeeRecord);
        });
        console.log(employeeDataNew);
        console.log('All completed!');
    } catch (error) {
        console.error(error);
    }
}

create_Data();