let students = [
    {"id":"1001",'firstname':'Luke','lastname':'Skywalker'},
    {"id":"1002",'firstname':'Tony','lastname':'Stark'},
    {"id":"1003",'firstname':'Somchai','lastname':'Jaidee'},
    {"id":"1004",'firstname':'Monkey D','lastname':'Luffee'},
];
let company = [
    {"id":"1001","company":"Walt Disney"},
    {"id":"1002","company":"Marvel"},
    {"id":"1003","company":"Love2work"},
    {"id":"1004","company":"One Piece"},
];
let salary = [
    {"id":"1001","salary":"40000"},
    {"id":"1002","salary":"1000000"},
    {"id":"1003","salary":"20000"},
    {"id":"1004","salary":"9000000"},
];
let like = [
    {"id":"1001","like":"apple"},
    {"id":"1002","like":"banana"},
    {"id":"1003","like":"orange"},
    {"id":"1004","like":"papaya"},
];
let dislike = [
    {"id":"1001","dislike":"banana"},
    {"id":"1002","dislike":"orange"},
    {"id":"1003","dislike":"papaya"},
    {"id":"1004","dislike":"apple"},
];

let employeesDatabase = [];

//หาค่า id 
let studentsID = {};

function set_studentsID(arrData) {
    for (let i = 0; i < arrData.length; i++) {
        for (let x in arrData[i]) {
            if (x == 'id') {
                studentsID[x + arrData[i][x]] = arrData[i][x];
            }
        }
    }    
}
set_studentsID(students);
set_studentsID(company);
set_studentsID(salary);
set_studentsID(like);
set_studentsID(dislike);

for (let x in studentsID) {
    let employeesID = {};

    employeesID['id'] = studentsID[x];
    employeesDatabase.push(employeesID);
}    

//บันทึกรายละเอียด
for (let i = 0; i < employeesDatabase.length; i++) {
    for (let x in employeesDatabase[i]) {
        function findID(arrData) { 
            return arrData.id === employeesDatabase[i].id;
        }
        function setData_employeesDatabase(arrData) {
            let arrDataRecord = arrData.find(findID);
            for (let y in arrDataRecord) {
                employeesDatabase[i][y] = arrDataRecord[y];
            }
        }
        
        setData_employeesDatabase(students);
        setData_employeesDatabase(company);
        setData_employeesDatabase(salary);
        setData_employeesDatabase(like);
        setData_employeesDatabase(dislike);
    }    
}

//บันทึกลง File
const fs = require('fs');

let JSON_employeesDatabase = JSON.stringify(employeesDatabase);

function write_dataFile(Filename, data) {
    return new Promise(function(resolve, reject) {
        fs.writeFile(Filename, data, 'utf8', function(err) {
            if (err)
                reject(err);
            else
                resolve();
        });
    });
}
async function createFile() {
    try {
        await write_dataFile('homework3-3.json', JSON_employeesDatabase);

        console.log('All completed!');
    } catch (error) {
        console.error(error);
    }
}

createFile();