const Router = require('koa-router');
const router = new Router();
const userController = require('./controller/user');

router.get('/', async(ctx) => {
    ctx.body = "homepage";
})
.get('/register', userController.register)
.post('/register_completed', userController.registerCompleted)
.post('/register_completed_ajax', userController.registerCompletedAjax)
.get('/login', userController.login)
.post('/login_completed', userController.loginCompleted)
.get('/my_profile', userController.myProfile)
.get('/logout', userController.logout)
.get('/admin', userController.admin);

module.exports = router;