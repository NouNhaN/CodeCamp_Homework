const Koa = require("Koa");
const session = require("koa-session");
const render = require("koa-ejs");
const path = require("path");
const app = new Koa();
const formidable = require("koa2-formidable");
const userModel = require("./model/user_model");
const router = require("./routes");

render(app, {
  root: path.join(__dirname, "view"),
  layout: "template",
  viewExt: "ejs",
  cache: false,
  debug: true
});

app.keys = ["asdf;lkdsa naksldflkdsajflkdsa"];
app.use(formidable());

const sessionStore = {};

const CONFIG = {
  key: "koa:sess" /** (string) cookie key (default is koa:sess) */,
  /** (number || 'session') maxAge in ms (default is 1 days) */
  /** 'session' will result in a cookie that expires when session/browser is closed */
  /** Warning: If a session cookie is stolen, this cookie will never expire */
  maxAge: 60 * 60 * 1000,
  overwrite: true /** (boolean) can overwrite or not (default true) */,
  httpOnly: true /** (boolean) httpOnly or not (default true) */,
  store: {
    get(key, maxAge, { rolling }) {
      return sessionStore[key];
    },
    set(key, sess, maxAge, { rolling, changed }) {
      sessionStore[key] = sess;
    },
    destroy(key) {}
  }
};

app.use(session(CONFIG, app));

app.use(async (ctx, next) => {
  if (ctx.path === "/login_completed" || ctx.path === "/login" || ctx.path === '/register') {
    await next();
    return;
  }

  if (!ctx.session || (ctx.session && !ctx.session.userId)) {
    await ctx.render("unAuth");
  } else {
    let userRow = await userModel.getUserInfoByUserId(ctx.session.userId);
    if (userRow.role != "admin" && ctx.path == "/admin") {
      ctx.body = "Forbidden!!";
    } else {
      await next();
    }
  }
});

app.use(router.routes()).use(router.allowedMethods());

app.listen(3000);
