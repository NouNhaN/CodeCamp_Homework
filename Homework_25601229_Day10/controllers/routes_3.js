const path = require('path');
const render = require('koa-ejs');              //npm install koa-ejs

const user = require('../models/homework10_3');

module.exports = function(app) {
    render(app, {
        root: path.join(__dirname, '../views'),
        layout: 'homework10-3',
        viewExt: 'ejs',
        cache: false,
        debug: false
    });

    app.use(async (ctx, next) => {
        try {
            let data = await user.getAllUser();

            await ctx.render('homework10-3', {
                "data" : data
            });
            await next();
        } catch (err) {
           ctx.status = 400
           ctx.body = `Uh-oh: ${err.message}`
        }
    });
}