const db = require('../lib/db_3');

class User {

    async getAllUser() {
        let data = await db.execute('SELECT * FROM user')
        return data;
    }
}

module.exports = new User();