const path = require('path');
const koa = require('koa');                     //npm install koa
const render = require('koa-ejs');              //npm install koa-ejs
const logger = require('koa-logger');           //npm install koa-logger
const mysql = require('mysql2/promise');

let app = new koa();

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'homework10-1',
    viewExt: 'ejs',
    cache: false,
    debug: true
});

async function getData() {
    try {
        // create the connection
        const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'codecamp'});
        // query database
        const [rows, fields] = await connection.execute('SELECT * FROM user');

        return rows;
//        console.log(rows);
    } catch (err) {
//        console.log(err);
    };
};    

app.use(async (ctx, next) => {
    try {
        let data = await getData();

        await ctx.render('homework10-1', {
            "data" : data
        });
        await next();
    } catch (err) {
       ctx.status = 400
       ctx.body = `Uh-oh: ${err.message}`
    }
});

app.listen(3000);

app.use(logger());