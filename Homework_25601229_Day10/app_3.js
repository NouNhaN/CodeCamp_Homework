const koa = require('koa');                     //npm install koa
const logger = require('koa-logger');           //npm install koa-logger

const app = new koa();

const otherMiddleware = require('./controllers/routes_3')(app);

app.listen(3000);

//app.use(logger());