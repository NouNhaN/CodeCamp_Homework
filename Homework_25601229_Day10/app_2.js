const path = require('path');
const koa = require('koa');                     //npm install koa
const render = require('koa-ejs');              //npm install koa-ejs
const logger = require('koa-logger');           //npm install koa-logger

const db = require('./lib/db_2');

const app = new koa();

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'homework10-2',
    viewExt: 'ejs',
    cache: false,
    debug: true
});

app.use(async (ctx, next) => {
    try {
        let data = await db.execute('SELECT * FROM user');

        await ctx.render('homework10-2', {
            "data" : data[0]
        });
        await next();
    } catch (err) {
       ctx.status = 400
       ctx.body = `Uh-oh: ${err.message}`
    }
});

app.listen(3000);

app.use(logger());