class Database {
    constructor() {
        this.connectDatabase();
    }
    async connectDatabase() {
        const mySql = require('mysql2/promise');
        //Create database connection
        this.connection = await mySql.createConnection({host:'localhost', user: 'root', database: 'codecamp'});
    }
    execute(sql) { 
       return this.connection.execute(sql);
    }
 }

module.exports = new Database();

