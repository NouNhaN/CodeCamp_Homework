const mySql = require('mysql2/promise');
const config = require('../config/database');

class Database {
    constructor() {
        this.connectDatabase();
    }
    async connectDatabase() {
        //Create database connection
        this.connection = await mySql.createConnection(
            {
                host: config.host, 
                user: config.user, 
                database: config.database
            });
    }
   async execute(sql) { 
        let data = await (this.connection.execute(sql));
        return data[0];
    }
}

module.exports = new Database();

