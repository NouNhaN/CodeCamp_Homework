const fs = require('fs');

function read_dataFile(Filename) {
    return fs.readFileSync(Filename,'utf8');
}

function addYearSalary(row) {
    let addYearSalaryRecord = {};

    for (let key in row) {
        addYearSalaryRecord[key] = row[key];
        if (key == 'salary') {
            addYearSalaryRecord.yearSalary = Number(row[key]) * 12;
        }
    }

    return addYearSalaryRecord;
}

function addNextSalary(row) {
    let addNextSalaryRecord = {};
    let nextSalary = [];
    let salary = 0;

    for (let key in row) {
        addNextSalaryRecord[key] = row[key];
        if (key == 'salary') {
            salary = Number(row.salary);
            nextSalary.push(salary);
            for (let i = 1; i < 3; i++) {
                salary = salary + (salary * (10/100));  
                nextSalary.push(salary);
            }

            addNextSalaryRecord.nextSalary = nextSalary;
        }
    }

    return addNextSalaryRecord;
}

function addAdditionalFields(data) {
    newEmployees = [];

    for (let i = 0; i < data.length; i++) {
        newEmployeesRecord = addYearSalary(data[i]);    
        newEmployeesRecord = addNextSalary(newEmployeesRecord);

        newEmployees.push(newEmployeesRecord);
    }

    return newEmployees;
}

function create_Data_homework7_2() {
    try {
        const JSON_Data = read_dataFile('./homework7.json');
        const employees = JSON.parse(JSON_Data);

        let newEmployees = addAdditionalFields(employees);

        newEmployees[0].salary = 0;

        console.log(employees);
        console.log(newEmployees);

        console.log(employees[0].salary);
        console.log(newEmployees[0].salary);
    } catch (error) {
        console.error(error);
    }
}
       
create_Data_homework7_2();

module.exports = {
    read_dataFile,
    addYearSalary,
    addNextSalary
};