const fs = require('fs');

function clone_Object(objData) {
    if (typeof objData === "object") {
        if (objData.length > 0) {
            let new_objData_arr = [];
            let objData_arr;
            for (let i = 0; i < objData.length; i++) {
                objData_arr = clone_Object(objData[i]);
                new_objData_arr.push(objData_arr);
            }

            return new_objData_arr;
        } else {
            let new_objData_obj = {};
            for (let key in objData) {
                new_objData_obj[key] = clone_Object(objData[key]);
            }
            return new_objData_obj;
        }
    } else {
        return objData;
    };
}

function create_Data_homework7_3() {
    try {
        let input_Data_1 = [1, 2, 3];
        let input_Data_2 = { a: 1, b: 2 };
        let input_Data_3 = [1, 2, { a: 1, b: 2 }];
        let input_Data_4 = [1, 2, { a: 1, b: { c: 3, d: 4 } }];

        let new_Data_1 = clone_Object(input_Data_1);

        new_Data_1[0] = 0;
        console.log(input_Data_1);
        console.log(new_Data_1);

        let new_Data_2 = clone_Object(input_Data_2);

        new_Data_2.a = 0;
        console.log(input_Data_2);
        console.log(new_Data_2);

        let new_Data_3 = clone_Object(input_Data_3);

        new_Data_3[0] = 0;
        new_Data_3[2].a = 0;
        console.log(input_Data_3);
        console.log(new_Data_3);

        let new_Data_4 = clone_Object(input_Data_4);

        new_Data_4[0] = 0;
        new_Data_4[2].a = 0;
        new_Data_4[2].b.c = 0;
        console.log(input_Data_4);
        console.log(new_Data_4);  
    } catch (error) {
        console.error(error);
    }
}

create_Data_homework7_3();

module.exports = {
    clone_Object
};