const fs = require('fs');

function readPromise_dataFile(Filename) {
    return new Promise((resolve, reject) => {
        fs.readFile(Filename, 'utf8', (err, dataFile) => {
            if (err) reject(err);
            else resolve(dataFile);
        });
    });
}

function addYearSalary(row) {
    return new Promise((resolve, reject) => {
        if (row == '') reject('err');
        else {
            row.yearSalary = row.salary * 12;
            resolve(row);
        }
    });
}

function addNextSalary(row, year) {
    return new Promise((resolve, reject) => {
        if (row == '' || year <= 0) reject('err');
        else {
            let nextSalary = [];
            let salary = 0;

            salary = Number(row.salary);
            nextSalary.push(salary);
            for (let i = 0; i < year - 1; i++) {
                salary = salary + (salary * (10/100));  
                nextSalary.push(salary);
            }

            row.nextSalary = nextSalary;
            resolve(row);
        }
    });
}

function addAdditionalFields(data) {
    return new Promise((resolve, reject) => {
        if (data == '') reject('err');
        else {
            data.forEach(async (currentValue_employees, index_employees, arr_employees) => {
                await addYearSalary(currentValue_employees);
                await addNextSalary(currentValue_employees, 3);
            });    

            resolve(data);
        }
    });
}

async function create_Data_homework7_1() {
    try {
        const JSON_Data = await readPromise_dataFile('./homework7.json');
        const employees = JSON.parse(JSON_Data);

        await addAdditionalFields(employees);

        console.log(employees);
    } catch (error) {
        console.error(error);
    }
}
       
create_Data_homework7_1();

module.exports = {
    readPromise_dataFile,
    addYearSalary,
    addNextSalary,
    addAdditionalFields
};