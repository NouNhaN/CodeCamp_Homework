import React, { Component } from "react";
import { Layout } from 'antd';
import Header from "./Header.js";
import Main from "./Main.js";
import Footer from "./Footer.js";
import "../css/App.css";

class App extends Component {
  render() {
    return (
      <Layout>
        <Header />
        <Main />
        <Footer />
      </Layout>
    );
  }
}

export default App;
