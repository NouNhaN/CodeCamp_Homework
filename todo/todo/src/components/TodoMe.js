import React, { Component } from "react";
import { Divider, List, Input, Icon, Button } from "antd";

export default class TodoMe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputText: "",
      todoLists: []
    };
  }

  componentDidMount() {
    this.fetchGet();
  }

  async fetchGet() {
    const result = await fetch("http://localhost:8000/todo");
    let data = await result.json();
    // console.log(data);
    // let todoLists = data.map((value, index) => {
    //   return value.text;
    // });
    this.setState({ todoLists: data });
    // console.log(this.state.todoLists);
  }

  async fetchPost(string) {
    const result = await fetch("http://localhost:8000/todo", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        content: string
      })
    });

    if (result.ok) {
      // ท่านี้ก็ได้ดูดีกว่า 1
      // let data = await result.json();
      // let listItem = this.state.listItem.concat(data.contents);
      // this.setState({ listItem, isLoading: false });

      // ท่านี้ก็ได้ดูดีกว่า 2
      this.fetchGet();
    }
  }

  async fetchDelete(id) {
    const result = await fetch("http://localhost:8000/todo/" + id, {
      method: "DELETE",
      headers: {
        Accept: "applicatin/json",
        "Content-Type": "application/json"
      }
      // body: JSON.stringify({
      //   text: string
      // })
    });

    if (result.ok) {
      this.fetchGet();
    }
  }

  addTodo = () => {
    this.fetchPost(this.state.inputText);
    this.setState({
      inputText: ""
    });
  };

  setInputText = event => {
    this.setState({ inputText: event.target.value });
  };

  render() {
    return (
      <div style={{ overflow: "auto" }}>
        <h1>Todo My Api</h1>
        <Divider />
        <div style={{}}>
          <List
            size="large"
            header={
              <div>
                <Input.Search
                  size="large"
                  onChange={this.setInputText}
                  onPressEnter={this.addTodo}
                  enterButton={<Icon type="plus" />}
                  prefix={<Icon type="enter" />}
                  placeholder="Input new todo then press enter to add a new one."
                  onSearch={this.addTodo}
                  value={this.state.inputText}
                />
              </div>
            }
            bordered
            dataSource={this.state.todoLists}
            renderItem={item => (
              <List.Item>
                <List.Item.Meta description={item.content} />
                <div>
                  <Button
                    size="small"
                    type="danger"
                    onClick={() => this.fetchDelete(item.id)}
                    icon="minus"
                    shape="circle"
                  />
                </div>
              </List.Item>
            )}
          />
        </div>
      </div>
    );
  }
}
