import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import { Layout, /* Breadcrumb */ } from 'antd';
import TodoMock from "./TodoMock";
import TodoMe from "./TodoMe";
import TodoFirebase from "./TodoFirebase";
const { Content } = Layout;

export default class Main extends Component {
  render() {
    return (
      <Content style={{ padding: "0 50px" }}>
        {/* <Breadcrumb style={{ margin: "16px 0" }}>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>List</Breadcrumb.Item>
          <Breadcrumb.Item>App</Breadcrumb.Item>
        </Breadcrumb> */}
        <div style={{ marginTop: "32px", background: "#fff", padding: 24, minHeight: "83vh" }}>
          <Switch>
            <Route exact path="/" component={TodoMock} />
            <Route exact path="/todoMe" component={TodoMe} />
            <Route exact path="/todoFirebase" component={TodoFirebase} />
          </Switch>
        </div>
      </Content>
    );
  }
}
