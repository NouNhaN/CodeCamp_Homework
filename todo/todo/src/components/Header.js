import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Layout, Menu } from 'antd';
const { Header } = Layout;

export default class NavBar extends Component {
  render() {
    return (
      <Header>
        <div className="logo" />
        <Menu
          theme="dark"
          mode="horizontal"
          // defaultSelectedKeys={["1"]}
          style={{ lineHeight: "64px" }}
        >
          <Menu.Item key="1"><Link to="/">Todos (Mock Api)</Link></Menu.Item>
          <Menu.Item key="2"><Link to="/TodoMe">Todos (My Api)</Link></Menu.Item>
          <Menu.Item key="3"><Link to="/TodoFirebase">Todos (Firebase)</Link></Menu.Item>
        </Menu>
      </Header>
    );
  }
}
