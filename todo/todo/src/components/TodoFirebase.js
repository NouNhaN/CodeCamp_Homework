import React, { Component } from "react";
import { Divider, List, Input, Icon, Button } from "antd";
import * as firebase from "firebase";

const config = {
  apiKey: "AIzaSyBuu4L4mqDURk4CN_pAq5Uwb5R8swehUrM",
  authDomain: "todo-28473.firebaseapp.com",
  databaseURL: "https://todo-28473.firebaseio.com",
  projectId: "todo-28473",
  storageBucket: "todo-28473.appspot.com",
  messagingSenderId: "203500436449"
};
firebase.initializeApp(config);
const database = firebase.database();

export default class TodoFirebase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputText: "",
      todoLists: []
    };
  }

  componentDidMount() {
    this.fetchGet();
  }

  async fetchGet() {
    const todoRef = database.ref("/");
    todoRef.on("value", snapshot => {
      const vals = snapshot.val();
      if (!vals) {
        return false;
      }
      const tempTodos = [];
      for (const key in vals) {
        tempTodos.push(vals[key]);
      }
      this.setState({ todoLists: tempTodos });
    });
  }

  async fetchPost(string) {
    const key = database.ref("/").push().key;
    database.ref("/" + key).set({
      id: key,
      content: string
    });
  }

  async fetchDelete(id) {
    const todoRef = database.ref("/" + id);
    todoRef.remove();
  }

  addTodo = () => {
    this.fetchPost(this.state.inputText);
    this.setState({
      inputText: ""
    });
  };

  setInputText = event => {
    this.setState({ inputText: event.target.value });
  };

  render() {
    return (
      <div style={{ overflow: "auto" }}>
        <h1>Todo Firebase</h1>
        <Divider />
        <div style={{}}>
          <List
            size="large"
            header={
              <div>
                <Input.Search
                  size="large"
                  onChange={this.setInputText}
                  onPressEnter={this.addTodo}
                  enterButton={<Icon type="plus" />}
                  prefix={<Icon type="enter" />}
                  placeholder="Input new todo then press enter to add a new one."
                  onSearch={this.addTodo}
                  value={this.state.inputText}
                />
              </div>
            }
            bordered
            dataSource={this.state.todoLists}
            renderItem={item => (
              <List.Item>
                <List.Item.Meta description={item.content} />
                <div>
                  <Button
                    size="small"
                    type="danger"
                    onClick={() => this.fetchDelete(item.id)}
                    icon="minus"
                    shape="circle"
                  />
                </div>
              </List.Item>
            )}
          />
        </div>
      </div>
    );
  }
}
