import React, { Component } from "react";
import { Layout } from "antd";
const { Footer } = Layout;

export default class FooterBar extends Component {
  render() {
    return (
      <Footer style={{ textAlign: "center" }}>
        Designed by: 
      </Footer>
    );
  }
}
