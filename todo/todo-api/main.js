const Koa = require("koa");
const Router = require("koa-router");
const bodyParser = require("koa-bodyparser");
const cors = require("@koa/cors")

const db = require("mysql2/promise").createPool({
  host: "localhost",
  user: "root",
  password: "",
  database: "todo"
});

const router = new Router()
  .get("/todo", getAllTodos)
  .get("/todo/:id", getTodo)
  .post("/todo", addTodo)
  .delete("/todo/:id", deleteTodo);

app = new Koa()
  .use(bodyParser())
  .use(cors())  
  .use(router.routes())
  .listen(8000);

async function getAllTodos(ctx) {
  const [result] = await db.execute(
    `SELECT * 
    FROM todos`
  );
  ctx.body = result;
}

async function getTodo(ctx) {
  const id = ctx.params.id;
  const [result] = await db.execute(
    `SELECT * FROM todos WHERE id = ?`, 
    [id]
  );
  ctx.body = result;
}

async function addTodo(ctx) {
  const { content } = ctx.request.body;
  const [result] = await db.execute(
    `INSERT INTO todos (content)
    VALUES (?)`, 
    [content]
  );
  if (result) {
    ctx.body = {};
  }
}

async function deleteTodo(ctx) {
  const id = ctx.params.id;
  const [result] = await db.execute(
    `DELETE FROM todos 
    WHERE id = ?`, 
    [id]
  );
  if (result) {
    ctx.body = {};
  }
}
