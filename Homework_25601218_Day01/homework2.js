let peopleSalary = [
    {
        'id': '1001',
        'firstname': 'Luke',
        'lastname': 'Skywalker',
        'company': 'Walt Disney',
        'salary': '40000'
    },
    {
        'id': '1002',
        'firstname': 'Tony',
        'lastname': 'Stark',
        'company': 'Marvel',
        'salary': '1000000'
    },
    {
        'id': '1003',
        'firstname': 'Somchai',
        'lastname': 'Jaidee',
        'company': 'Love2work',
        'salary': '20000'
    },
    {
        'id': '1004',
        'firstname': 'Monkey D',
        'lastname': 'Luffee',
        'company': 'One Piece',
        'salary': '9000000'
    },
]

let peopleSalaryNew = [];

for (let index = 0; index < peopleSalary.length; index++) {
    let peopleSalaryText = peopleSalary[index];
    let peopleSalaryRecord = {};

    for (let key in peopleSalaryText) {
        if (key != 'company') {
            peopleSalaryRecord[key] = peopleSalaryText[key];
        }
    }

    peopleSalaryNew.push(peopleSalaryRecord);
}
console.log(peopleSalaryNew); 