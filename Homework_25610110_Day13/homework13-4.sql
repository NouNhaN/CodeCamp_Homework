
------- หารายได้เฉลี่ยของแต่ละตำแหน่ง
            SELECT t.title, AVG(s.salary) AS avg_salary
            FROM (titles AS t
                INNER JOIN salaries s ON s.emp_no = t.emp_no)       
            GROUP BY t.title
            ORDER BY t.title;
/*
            +--------------------+------------+
            | title              | avg_salary |
            +--------------------+------------+
            | Assistant Engineer | 59304.9863 |
            | Engineer           | 59508.0397 |
            | Manager            | 66924.2706 |
            | Senior Engineer    | 60543.2191 |
            | Senior Staff       | 70470.8353 |
            | Staff              | 69309.1023 |
            | Technique Leader   | 59294.3742 |
            +--------------------+------------+
*/            
------- ตำแหน่งไหนได้รายได้ มากสุด, น้อยสุด
            SELECT '1 - Min Salary' AS salary_type, t.title, MIN(s.salary) AS salary
            FROM (titles AS t
                INNER JOIN salaries s ON s.emp_no = t.emp_no)   
			WHERE s.salary <= (SELECT MIN(s_1.salary) FROM salaries AS S_1)
            GROUP BY t.title
            
            UNION

            SELECT '2 - Max Salary' AS salary_type, t.title, MAX(s.salary) AS salary
            FROM (titles AS t
                INNER JOIN salaries s ON s.emp_no = t.emp_no)   
			WHERE s.salary >= (SELECT MAX(s_1.salary) FROM salaries AS S_1)
            GROUP BY t.title

            ORDER BY salary_type;
/*
            +----------------+------------------+--------+
            | salary_type    | title            | salary |
            +----------------+------------------+--------+
            | 1 - Min Salary | Technique Leader |  38623 |
            | 2 - Max Salary | Senior Staff     | 158220 |
            | 2 - Max Salary | Staff            | 158220 |
            +----------------+------------------+--------+
*/            
------- แต่ละตำแหน่งมีพนักงานกี่คน
            SELECT t.title, COUNT(s.emp_no) AS count_emp
            FROM (titles AS t
                INNER JOIN salaries s ON s.emp_no = t.emp_no)       
            GROUP BY t.title
            ORDER BY t.title;
/*
            +--------------------+-----------+
            | title              | count_emp |
            +--------------------+-----------+
            | Assistant Engineer |    143036 |
            | Engineer           |   1115673 |
            | Manager            |       388 |
            | Senior Engineer    |   1137450 |
            | Senior Staff       |   1078920 |
            | Staff              |   1019729 |
            | Technique Leader   |    143311 |
            +--------------------+-----------+
*/
