
------- นักเรียนแต่ละคน ซื้อคอร์สไปคนละเท่าไร จำนวนกี่คอร์ส
            SELECT s.name AS student_name, COUNT(e.course_id) AS student_course_count,  SUM(c.price) AS courses_price_sum
            FROM ((enrolls AS e 
                INNER JOIN courses AS c ON c.id = e.course_id)
				INNER JOIN students AS s ON s.id = e.student_id)
			GROUP BY s.name
            ORDER BY s.name;
/*
            +-----------------+----------------------+-------------------+
            | student_name    | student_course_count | courses_price_sum |
            +-----------------+----------------------+-------------------+
            | Student_Name_01 |                    1 |               120 |
            | Student_Name_02 |                    2 |               240 |
            | Student_Name_03 |                    1 |                30 |
            | Student_Name_04 |                    1 |                90 |
            | Student_Name_05 |                    1 |                90 |
            | Student_Name_06 |                    1 |                90 |
            | Student_Name_07 |                    1 |                90 |
            | Student_Name_08 |                    1 |                90 |
            | Student_Name_09 |                    1 |                90 |
            | Student_Name_10 |                    1 |               120 |
            +-----------------+----------------------+-------------------+
*/              
------- นักเรียนแต่ละคนซื้อคอร์สไหนราคาแพงสุด 
            SELECT s.name AS student_name, c.name AS course_name, c.price AS course_price 
            FROM ((enrolls AS e 
                INNER JOIN courses AS c ON c.id = e.course_id)
				INNER JOIN students AS s ON s.id = e.student_id)
            WHERE c.price >=    (   SELECT MAX(c_1.price) 
                                    FROM (enrolls AS e_1 INNER JOIN courses AS c_1 ON c_1.id = e_1.course_id)
                                    WHERE e_1.student_id = s.id
                                )
            GROUP BY s.name, c.name, c.price
			ORDER BY s.name, c.name;
/*
            +-----------------+-------------------------------------+--------------+
            | student_name    | course_name                         | course_price |
            +-----------------+-------------------------------------+--------------+
            | Student_Name_01 | Filmmaking                          |          120 |
            | Student_Name_02 | Filmmaking                          |          120 |
            | Student_Name_02 | JavaScript for Beginner             |          120 |
            | Student_Name_03 | Database System Concept             |           30 |
            | Student_Name_04 | Photography                         |           90 |
            | Student_Name_05 | Shooting, Ball Handler, and Scoring |           90 |
            | Student_Name_06 | Cooking #2                          |           90 |
            | Student_Name_07 | Electronic Music Production         |           90 |
            | Student_Name_08 | Screenwriting                       |           90 |
            | Student_Name_09 | Dramatic Writing                    |           90 |
            | Student_Name_10 | JavaScript for Beginner             |          120 |
            +-----------------+-------------------------------------+--------------+
*/            