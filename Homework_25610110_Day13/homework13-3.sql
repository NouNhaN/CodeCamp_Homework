
------- เติมตารางนี้ให้เต็ม สำหรับ MySql และหาว่า isolation level ไหน คือค่า default
------- https://natc12.wordpress.com/2010/03/22/isolation-level/
/*
        +------------------+-------------+----------------------+----------------+
        | Level            | Dirty reads | Non-repeatable reads | Phantoms reads |
        +------------------+-------------+----------------------+----------------+
        | read uncommitted |      O      |           O          |       O        |                      O - มีโอกาส
        | read committed   |      X      |           O          |       O        |                      X - ไม่มี
        | repeatable read  |      X      |           X          |       X        |
        | serializable     |      X      |           X          |       X        |
        +------------------+-------------+----------------------+----------------+
        | Defailt          |      X      |           X          |       X        | -> repeatable read เพราะไม่มีการ Lock ข้อมูล
        +------------------+-------------+----------------------+----------------+

------- read uncommitted
                    [1] [2] -   set transaction isolation level read uncommitted;
--------------- Dirty reads
                    [1] [2] -   begin;
                    [1]     -   select number from tests where id = 1;
                        [2] -   update tests set number = 9 where id = 1;
                    [1]     -   select number from tests where id = 1;    
                                ค่า number ใน [1] ***** เปลี่ยนแปลง *****
                    [1] [2] -   rollback;
--------------- Non-repeatable reads
                    [1] [2] -   begin;
                    [1]     -   select number from tests where id = 1;
                        [2] -   update tests set number = 99 where id = 1;
                    [1]     -   select number from tests where id = 1;    
                                ค่า number ใน [1] ***** เปลี่ยนแปลง *****
                        [2] -   commit;
                    [1]     -   select number from tests where id = 1;    
                                ค่า number ใน [1] ***** เปลี่ยนแปลง *****
                    [1]     -   rollback;
--------------- Phantoms reads
                    [1] [2] -   begin;
                    [1]     -   select count(*) from tests;
                        [2] -   insert into tests (id, number) values (9, 99);
                    [1]     -   select count(*) from tests;
                                จำนวน record ใน [1] ***** เปลี่ยนแปลง *****
                        [2] -   commit;
                    [1]     -   select count(*) from tests;    
                                จำนวน record ใน [1] ***** เปลี่ยนแปลง *****
                    [1]     -   rollback;
------- read committed
                    [1] [2] -   set transaction isolation level read committed;
--------------- Dirty reads
                    [1] [2] -   begin;
                    [1]     -   select number from tests where id = 1;
                        [2] -   update tests set number = 9 where id = 1;
                    [1]     -   select number from tests where id = 1;    
                                ค่า number ใน [1] ***** ไม่เปลี่ยนแปลง *****
                    [1] [2] -   rollback;
--------------- Non-repeatable reads
                    [1] [2] -   begin;
                    [1]     -   select number from tests where id = 1;
                        [2] -   update tests set number = 999 where id = 1;
                    [1]     -   select number from tests where id = 1;    
                                ค่า number ใน [1] ***** ไม่เปลี่ยนแปลง *****
                        [2] -   commit;
                    [1]     -   select number from tests where id = 1;    
                                ค่า number ใน [1] ***** เปลี่ยนแปลง *****
                    [1]     -   rollback;
--------------- Phantoms reads
                    [1] [2] -   begin;
                    [1]     -   select count(*) from tests;
                        [2] -   insert into tests (id, number) values (99, 999);
                    [1]     -   select count(*) from tests;
                                จำนวน record ใน [1] ***** ไม่เปลี่ยนแปลง *****
                        [2] -   commit;
                    [1]     -   select count(*) from tests;    
                                จำนวน record ใน [1] ***** เปลี่ยนแปลง *****
                    [1]     -   rollback;
------- repeatable read
                    [1] [2] -   set transaction isolation level repeatable read;
--------------- Dirty reads
                    [1] [2] -   begin;
                    [1]     -   select number from tests where id = 1;
                        [2] -   update tests set number = 9 where id = 1;
                    [1]     -   select number from tests where id = 1;    
                                ค่า number ใน [1] ***** ไม่เปลี่ยนแปลง *****
                    [1] [2] -   rollback;
--------------- Non-repeatable reads
                    [1] [2] -   begin;
                    [1]     -   select number from tests where id = 1;
                        [2] -   update tests set number = 9999 where id = 1;
                    [1]     -   select number from tests where id = 1;    
                                ค่า number ใน [1] ***** ไม่เปลี่ยนแปลง *****
                        [2] -   commit;
                    [1]     -   select number from tests where id = 1;    
                                ค่า number ใน [1] ***** ไม่เปลี่ยนแปลง *****
                    [1]     -   rollback;
--------------- Phantoms reads
                    [1] [2] -   begin;
                    [1]     -   select count(*) from tests;
                        [2] -   insert into tests (id, number) values (999, 9999);
                    [1]     -   select count(*) from tests;
                                จำนวน record ใน [1] ***** ไม่เปลี่ยนแปลง *****
                        [2] -   commit;
                    [1]     -   select count(*) from tests;    
                                จำนวน record ใน [1] ***** ไม่เปลี่ยนแปลง *****
                    [1]     -   rollback;
------- serializable
                    [1] [2] -   set transaction isolation level serializable;
--------------- Dirty reads
                    [1] [2] -   begin;
                    [1]     -   select number from tests where id = 1;
                        [2] -   update tests set number = 9 where id = 1;
                                ไม่ทำการ Update ข้อมูลให้ ( update ให้ต่อเมื่อ [1] ทำการ commit หรือ Rollback )
                    [1]     -   select number from tests where id = 1;    
                                ค่า number ใน [1] ***** ไม่เปลี่ยนแปลง *****
                    [1] [2] -   rollback;
--------------- Non-repeatable reads
                    case - 1
                        [1] [2] -   begin;
                        [1]     -   select number from tests where id = 1;
                            [2] -   update tests set number = 99999 where id = 1;
                                        ไม่ทำการ Update ข้อมูลให้ ( update ให้ต่อเมื่อ [1] ทำการ commit หรือ Rollback )
                        [1]     -   rollback;    
                        [1]     -   select number from tests where id = 1;    
                                        ค่า number ใน [1] ***** ไม่เปลี่ยนแปลง *****
                            [2] -   commit;
                        [1]     -   select number from tests where id = 1;    
                                        ค่า number ใน [1] ***** เปลี่ยนแปลง *****
                        [1]     -   rollback;
                    case - 2
                        [1] [2] -   begin;
                        [1]     -   select number from tests where id = 1;
                            [2] -   update tests set number = 99999 where id = 1;
                                        ไม่ทำการ Update ข้อมูลให้ ( update ให้ต่อเมื่อ [1] ทำการ commit หรือ Rollback )
                        [1]     -   rollback;    
                        [1]     -   begin;    
                        [1]     -   select number from tests where id = 1;    
                                        ค่า number ใน [1] ***** ไม่เปลี่ยนแปลง *****
                            [2] -   commit;
                        [1]     -   select number from tests where id = 1;    
                                        ค่า number ใน [1] ***** ไม่เปลี่ยนแปลง *****
                        [1]     -   rollback;
--------------- Phantoms reads
                    [1] [2] -   begin;
                    [1]     -   select count(*) from tests;
                        [2] -   insert into tests (id, number) values (9999, 99999);
                    [1]     -   select count(*) from tests;
                                จำนวน record ใน [1] ***** ไม่เปลี่ยนแปลง *****
                        [2] -   commit;
                    [1]     -   select count(*) from tests;    
                                จำนวน record ใน [1] ***** ไม่เปลี่ยนแปลง *****
                    [1]     -   rollback;
*/

