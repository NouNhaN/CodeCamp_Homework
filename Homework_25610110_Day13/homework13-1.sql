
------- จากคอร์สที่มีคนเรียนทั้งหมด ได้เงินเท่าไร
            SELECT SUM(c.price) price_sum
            FROM (enrolls AS e 
                INNER JOIN courses AS c ON c.id = e.course_id);
/*
            +-----------+
            | price_sum |
            +-----------+
            |       700 |
            +-----------+
*/
------- นักเรียนแต่ละคน ซื้อคอร์สไปคนละเท่าไร
            SELECT s.name AS students_name, SUM(c.price) AS courses_price_sum
            FROM ((enrolls AS e 
                INNER JOIN courses AS c ON c.id = e.course_id)
				INNER JOIN students AS s ON s.id = e.student_id)
			GROUP BY s.name
            ORDER BY s.name;
/*
            +-----------------+-------------------+
            | students_name   | courses_price_sum |
            +-----------------+-------------------+
            | Student_Name_01 |                90 |
            | Student_Name_02 |                20 |
            | Student_Name_03 |                30 |
            | Student_Name_04 |                90 |
            | Student_Name_05 |                90 |
            | Student_Name_06 |                90 |
            | Student_Name_07 |                90 |
            | Student_Name_08 |                90 |
            | Student_Name_09 |                90 |
            | Student_Name_10 |                20 |
            +-----------------+-------------------+
*/            