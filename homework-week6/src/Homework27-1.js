import React, { Component } from 'react';
import { Layout, Button, Input, List, Avatar } from 'antd';

import './Homework27-1.css';

const { Header, Content, Footer } = Layout;

class Homework27_1 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputText : '',
      listItem: [],
      isMe: false
    };

    this.handleChangeText = this.handleChangeText.bind(this);
  };

  submitList = () => {
    let alignText = '';
    let nameText = '';
    let isMe = false;

    if (!this.state.isMe) {
      alignText = 'right';
      nameText = 'Me';
      isMe = true;
    } else {
      if (this.state.isMe) {
        alignText = 'left';
        nameText = 'Anonymous';
        isMe = false;
      } else {
        alignText = 'right';
        nameText = 'Me';
        isMe = true;
      };
    };

    if (this.state.inputText !== '') {
      let dataChat = {
        nameText: nameText,
        message: this.state.inputText,
        alignText: alignText
      }
      
      this.setState({
        listItem: this.state.listItem.concat([dataChat]),
        inputText: '',
        isMe: isMe
      })

      console.log('state=>', this.state)
    }
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.submitList();
    }
  }
  
  handleChangeText = (event) => {
    this.setState({inputText: event.target.value});
  };

  render() {
    return (
      <Layout>
        <Header style={{ background: '#fff', padding: 0 }}>
          <h1 style={{ textAlign: 'center' }} > Chat Box </h1>        
        </Header>
        <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
          <div style={{ padding: 24, background: '#fff', textAlign: 'center' }}>
            <List
              itemLayout="horizontal"
              dataSource={this.state.listItem}
              renderItem={item => (
                <List.Item>
                  <List.Item.Meta className={item.alignText}                  
                    avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                    title={item.nameText}
                    description={item.message}
                  />
                </List.Item>
              )}
            />
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          <div style={{ marginBottom:'10px' }}>
            <Input type="text" style={{ width: '65%' }} 
              onChange={this.handleChangeText}
              value={this.state.inputText}
              onKeyPress={this.handleKeyPress}
            />          
            <Button type="primary" htmlType="submit" onClick={this.submitList}> Send </Button>
          </div>
        </Footer>
      </Layout>
    );
  };
};

export default Homework27_1;