import React, { Component } from 'react';
import { Form, Icon, Input, Button, Layout, Row, Col } from 'antd'

import './Homework26-3.css';

const { Footer } = Layout;

class Homework263 extends Component {
  render() {
    return (
      <div>
        <Row>
          <Col className='left' span={12}>
            <div style={{ color: 'white', fontSize: '150%', paddingLeft: 100 }} className='left-div'>
              <div>
                <Icon type="search" /> Follow your interests. <br /><br />
                <Icon type="usergroup-add" /> Hear what people are talking about. <br /><br />
                <Icon type="message" /> Join the conversation.
              </div>
            </div>
          </Col>
          
          <Col span={12}>
            <Row>
              <Col span={24} style={{ textAlign: 'center', paddingTop: '2em' }}>
                <Form layout="inline" onSubmit={this.handleSubmit}>
                  <Form.Item>
                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                  </Form.Item>
                  <Form.Item>
                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                  </Form.Item>
                  <Form.Item>
                    <Button type="primary" htmlType="submit" ghost> Log in </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>

            <Row>
              <Col span={24} style={{ textAlign: 'center', paddingTop: '2em' }}>
                <div style={{ width: 400, marginLeft: 'auto', marginRight: 'auto', marginTop: 200, textAlign: 'left' }}>
                  <Icon type="twitter" style={{ color: '#1DA1F2', fontSize: '300%' }} />
                  <h1>See what's happening in <br/> the world right now <br/></h1>
                  <h3>Join Twitter today. <br/><br/></h3>
                  <Form onSubmit={this.handleSubmit} className="login-form">
                    <Form.Item>
                      <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                    </Form.Item>
                    <Form.Item>
                      <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                    </Form.Item>
                    <Form.Item>
                      <Button type="primary" htmlType="submit" className="login-form-button">
                        Get Started
                      </Button>
                      &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                      Have an account? <a href="">Login</a>
                    </Form.Item>
                  </Form>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>

        <Footer className='footer' style={{ textAlign: 'center' }}>
          About&emsp;
          Help Center&emsp;
          Blog&emsp;
          Status&emsp;
          Jobs&emsp;
          Terms&emsp;
          Privacy Policy&emsp;
          Cookies&emsp;
          Ads info&emsp;
          Brand&emsp;
          Apps&emsp;
          Advertise&emsp;
          Marketing&emsp;
          Businesses&emsp;
          Developers&emsp;
          Directory&emsp;
          Settings&emsp;
          © 2018 Twitter
        </Footer>
      </div>
    );
  };
};

export default Homework263;