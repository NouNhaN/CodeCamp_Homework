import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
// import App from './App';
// import Homework261 from "./Homework26-1";
// import Homework262 from './Homework26-2';
// import Homework263 from './Homework26-3';
import Homework271 from './Homework27-1';
// import Homework281 from './Homework28-1';
//import Todo from './Todo';

import registerServiceWorker from "./registerServiceWorker";

// ReactDOM.render(<App />, document.getElementById('root'));
// ReactDOM.render(<Homework261 />, document.getElementById("root"));
// ReactDOM.render(<Homework262 />, document.getElementById('root'));
// ReactDOM.render(<Homework263 />, document.getElementById('root'));
ReactDOM.render(<Homework271 />, document.getElementById('root'));
// ReactDOM.render(<Homework281 />, document.getElementById('root'));
//ReactDOM.render(<Todo />, document.getElementById('root'));

registerServiceWorker();
