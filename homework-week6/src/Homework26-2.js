import React, { Component } from 'react';
import { Layout, Menu, Carousel, Card, Row, Col, Rate } from 'antd';

import './Homework26-2.css';

const { Header, Content, Footer } = Layout;

class Homework26_2 extends Component {
  render() {
    return (
      <Layout>
        <Header>
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']} style={{ lineHeight: '64px' }}>
            <Menu.Item key="1"> Product </Menu.Item>
            <Menu.Item key="2"> About </Menu.Item>
          </Menu>
        </Header>

        <Content style={{ padding: '0 0', marginTop: 64 }}>
          <Carousel autoplay>
            <div><h3> 1 </h3></div>
            <div><h3> 2 </h3></div>
            <div><h3> 3 </h3></div>
            <div><h3> 4 </h3></div>
          </Carousel>

          <div className="gutter-example">
            <Row gutter={16}>
              <Col span={6}>
                <Card>
                  <div className="custom-image">
                    <img alt="example" width="100%" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />
                  </div>
                  <div className="custom-card">
                    <h3> Europe Street beat </h3>
                    <p >www.instagram.com </p>
                    <p><Rate /></p>
                  </div>
                </Card>
              </Col>
              <Col span={6}>
                <Card>
                  <div className="custom-image">
                    <img alt="example" width="100%" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />
                  </div>
                  <div className="custom-card">
                    <h3> Europe Street beat </h3>
                    <p> www.instagram.com </p>
                    <p><Rate /></p>
                  </div>
                </Card>
              </Col>
              <Col span={6}>
                <Card>
                  <div className="custom-image">
                    <img alt="example" width="100%" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />
                  </div>
                  <div className="custom-card">
                    <h3> Europe Street beat </h3>
                    <p> www.instagram.com </p>
                    <p><Rate /></p>
                  </div>
                </Card>
              </Col>
              <Col span={6}>
                <Card>
                  <div className="custom-image">
                    <img alt="example" width="100%" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />
                  </div>
                  <div className="custom-card">
                    <h3> Europe Street beat </h3>
                    <p > www.instagram.com </p>
                    <p><Rate /></p>
                  </div>
                </Card>
              </Col>
            </Row>
          </div>          
        </Content>

        <Footer style={{ textAlign: 'center' }}>
          สวยไหม? #ยังหรอก
        </Footer>        
      </Layout>      
    );
  };
};

export default Homework26_2;