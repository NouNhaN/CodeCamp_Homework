import React, { Component } from "react";

import { Input, Icon, Button, Card, List } from "antd";

// const URL = 'http://localhost:3030/api/todo/';

class Todo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputText: "",
      listItem: []
    };

    this.handleChangeText = this.handleChangeText.bind(this);
    this.submitList = this.submitList.bind(this);
  }

  // componentDidMount () {
  //   this.getTodos();
  // }

  // async fetchAsync (url, opts) {
  //   this.setState({isLoading : true});

  //   const resp = await fetch(url, opts);
  //   if (resp.status !== 200) {
  //     return;
  //   }

  //   this.setState({isLoading : false});
  //   return resp.json();
  // }

  // async getTodos () {
  //   let todos = await this.fetchAsync(URL);
  //   todos = todos.map(todo => {
  //     return { ...todo, selected: false }
  //   });
  //   this.setState({ todos });
  // }

  async saveData(title) {
    // const newTodo = await this.fetchAsync(URL, {
    //   method: 'POST',
    //   headers: {
    //     Accept: 'application/json',
    //     'Content-Type': 'application/json',
    //   },
    //   body: JSON.stringify({
    //     title
    //   }),
    // });
    // if (newTodo) {
    //   listItem: [...this.state.listItem, newTodo]
    // });
    // }
    if (title !== "") {
      this.setState({
        listItem: this.state.listItem.concat([{ title }])
      });
    }
  }

  async deleteData (index) {
    console.log(this.state.listItem)
    console.log(index)
  //   const resp = await this.fetchAsync(URL + id, {
  //       method: 'DELETE'
  //     });
  //   if (resp) {
  //     const todos = this.state.todos.filter(todo =>
  //       todo.id !== id
  //     );
  //     this.setState({ todos });
  //   }

      // this.state.listItem.splice(id, 1);

      // const newListItem = this.state.listItem.filter(listItem =>
      //   listItem.id !== id
      // );
      // console.log(newListItem)
      // this.setState({ newListItem });
      let newListitem = [...this.state.listItem];
      newListitem.splice(index, 1);
      console.log(newListitem)
      this.setState({ 
        listItem: newListitem 
      });
  }

  async submitList() {
    this.saveData(this.state.inputText);
    this.setState({
      inputText: ""
    });
  }

  handleKeyPress = event => {
    if (event.key === "Enter") {
      this.submitList();
    }
  };

  handleChangeText = event => {
    this.setState({ inputText: event.target.value });
  };

  render() {
    return (
      <div>
        <Card style={{ width: 500, backgroundColor: this.props.myColor }}>
          <h1>To-do-list</h1>

          <div style={{ marginBottom: "10px" }}>
            <Input
              addonAfter={
                <Button type="primary" onClick={this.submitList}>
                  {" "} Add {" "}
                </Button>
              }
              onChange={this.handleChangeText}
              value={this.state.inputText}
              onKeyPress={this.handleKeyPress}
            />
          </div>

          <List
            bordered
            dataSource={this.state.listItem}
            style={{ height: 300, overflow: "auto" }}
            renderItem={(item, index) => (
              <List.Item
                actions={[
                  <a onClick={() => this.deleteData(index)}>
                    <Icon
                      type="close-circle"
                      style={{ fontSize: 16, color: "rgb(255, 145, 0)" }}
                    />
                  </a>
                ]}
              >
                <h4> {item.title} </h4>
              </List.Item>
            )}
          />
        </Card>
      </div>
    );
  }
}

export default Todo;
