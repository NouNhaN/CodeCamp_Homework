import React, { Component } from "react";

import "./Todo.css";

import CompTodo from "./components/Todo";

class Todo extends Component {
  render() {
    return (
      <div className="App">
        <CompTodo />
      </div>
    );
  }
}

export default Todo;
