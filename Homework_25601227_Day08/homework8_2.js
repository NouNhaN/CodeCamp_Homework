const fs = require('fs');

class MobilePhone {
    constructor() {
    };

    PhoneCall() {};
    SMS() {};
    InternetSurfing() {};
};

class MobilePhone_SamSung_Galaxy_S8 extends MobilePhone {
    constructor() {
    };

    GooglePlay() {};
    UseGearVR() {};
    TransformToPC() {};
};

class MobilePhone_SamSung_Galaxy_Note_8 extends MobilePhone_SamSung_Galaxy_S8 {
    constructor() {
    };

    UsePen() {};
};

class MobilePhone_iPhone extends MobilePhone {
    constructor() {
    };

    AppStore() {};
};

class MobilePhone_iPhone8 extends MobilePhone_iPhone {
    constructor() {
    };

    TouchID() {};
};

class MobilePhone_iPhoneX extends MobilePhone_iPhone {
    constructor() {
    };

    FaceID() {};
};

module.exports = {
    homework8_2
};







