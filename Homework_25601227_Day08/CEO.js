const fs = require('fs');
const { Employee } = require('./Employee.js');

class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary);
        this.dressCode = 'suit';
        this.employeesRaw = [];
    };

    work(employee) {  // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    };
    _fire(employee) { // simulate private method
        this.dressCode = 'tshirt';
        console.log(employee.firstname + ' has been fired!' + ' Dress with :' + this.dressCode);
    };
    _hire(employee) { // simulate private method
        this.dressCode = 'tshirt';
        console.log(employee.firstname + ' has been hired back!' + ' Dress with :' + this.dressCode);
    };
    _seminar() { // simulate private method
        this.dressCode = 'suit';
        console.log("He is going to seminar." + " Dress with :" + this.dressCode);
    };
    _golf() { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);
    };

    gossip(employee, message) {
        console.log('Hey ' + employee.firstname + ', ' + message);
    };

    getSalary() {  // simulate public method
        return super.getSalary() * 2;
    };

    increaseSalary(employee, newSalary) {
        if (employee.setSalary(newSalary) == false) {
            console.log(employee.firstname + '\'s salary is less than before!!');
        } else {
            console.log(employee.firstname + '\'s salary has been set to ' + newSalary);
        };
    };

    _readPromise_dataFile(Filename) {
        return new Promise((resolve, reject) => {
            fs.readFile(Filename, 'utf8', (err, dataFile) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(dataFile);
                };
            });
        });
    }

    async readFileData(Filename, callbackFunction) {
        try {
            const JSON_Data = await this._readPromise_dataFile(Filename);
            callbackFunction(null, JSON.parse(JSON_Data));
        } catch (error) {
            console.error(error, null);
        }
    }

    talk(message) {
        console.log(message);
    };

    reportRobot() {

    };
}

module.exports = {
    CEO
};