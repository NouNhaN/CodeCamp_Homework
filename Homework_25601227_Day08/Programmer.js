const fs = require('fs');
const { Employee } = require('./Employee.js');


class Programmer extends Employee {
    constructor(firstname, lastname, salary, id, type) {
        super(firstname, lastname, salary);
        this.id = id;
        this.type = type; //'Backend/Frontend/Full Stack'
    };

    work() {  // simulate public method
        this._CreateWebsite();
        this._FixPC();
        this._InstallWindows();
    };
    _CreateWebsite() { // simulate private method
        console.log('CreateWebsite');
    };
    _FixPC() { // simulate private method
    };
    _InstallWindows() { // simulate private method
    };
          
    leaveForVacation(year, month, day) {
    };
    
    gossip(programmer, say) {
    };
}

module.exports = {
    Programmer
};