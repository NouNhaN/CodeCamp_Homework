const fs = require('fs');
const { Employee } = require('./Employee.js');
const { CEO } = require('./CEO.js');
const { Programmer } = require('./Programmer.js');

let somchai = new CEO("Somchai", "Sudlor", 30000);
somchai.readFileData('./homework8_3.json', (error, dataFile) => {
    somchai.employeesRaw = dataFile;

    let employees = [];
    for (i = 0; i < dataFile.length; i++) {
        employees[i] = new Programmer(
            dataFile[i].firstname, 
            dataFile[i].lastname, 
            dataFile[i].salary, 
            dataFile[i].id, 
            'Backend/Frontend/Full Stack'
        )
        somchai.employeesRaw[i] = employees[i];
    }

    console.log(employees);
});