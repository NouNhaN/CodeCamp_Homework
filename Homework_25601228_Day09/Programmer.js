const fs = require('fs');
const { Employee } = require('./Employee.js');

class Programmer extends Employee {
    constructor(firstname, lastname, salary, id, type) {
        super(firstname, lastname, salary);
        this.id = id;
        this.type = type; //'Backend/Frontend/Full Stack'
    };

    work() {  // simulate public method
        this._CreateWebsite();
        this._FixPC();
        this._InstallWindows();
    };
    _CreateWebsite() { // simulate private method
        console.log('work : CreateWebsite - ' + this.type);
    };
    _FixPC() { // simulate private method
        console.log('work : FixPC');
    };
    _InstallWindows() { // simulate private method
        console.log('work : InstallWindows');
    };
          
    leaveForVacation(year, month, day) {
    };
    
    gossip(Programmer, message) {
    };

    talk(message) {
        console.log(message);
    };
}

module.exports = {
    Programmer
};