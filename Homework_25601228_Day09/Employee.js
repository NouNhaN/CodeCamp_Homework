let fs = require('fs');

class Employee {
    constructor(firstname, lastname, salary) {
        this.firstname = firstname;
        this.lastname = lastname;
        this._salary = salary; // simulate private variable
    };

    setSalary(newSalary) { // simulate public method
        // return newSalary ถ้ามีเงินเดือนใหม่มีค่ามากกว่า this._salary
        // return false ถ้าเงินเดือนใหม่มีค่าน้อยกว่าเท่ากับ this._salary

        if (newSalary <= this._salary) {
            return false;
        } else {
            this._salary = newSalary;
            return this._salary;
        };
    };

    getSalary() {  // simulate public method
        return this._salary;
    };

    work(employee) {
        // leave blank for child class to be overidden
    };

    leaveForVacation(year, month, day) {

    };
};

module.exports = {
    Employee
};