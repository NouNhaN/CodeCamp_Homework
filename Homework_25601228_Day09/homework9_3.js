// https://lodash.com/docs

const _ = require('lodash');

function myAssign(object) {
    return _.assign(object);
}

function myTimes(n) {
    return _.times(n);
}

function myKeyBy(collection) {
    return _.keyBy(collection);
}

function myCloneDeep(value) {
    return _.cloneDeep(value);
}

function myFilter(collection) {
    return _.filter(collection);
}

function mySortBy(collection) {
    return _.sortBy(collection);
}
/* 
let xxx =  myCloneDeep([1,2]);
console.log(xxx); */