const fs = require('fs');
const { Employee } = require('./Employee.js');
const { CEO } = require('./CEO.js');
const { Programmer } = require('./Programmer.js');
const { OfficeCleaner } = require('./OfficeCleaner.js');

let somchai = new CEO("Somchai", "Sudlor", 30000);
let somsri = new OfficeCleaner("Somsri","Sudsuay",22000);

somchai.readFileData('./employee9.json', (error, dataFile) => {
    let employees = [];
    for (i = 0; i < dataFile.length; i++) {
        if (dataFile[i].role == 'Programmer') {
            employees[i] = new Programmer(
                dataFile[i].firstname, 
                dataFile[i].lastname, 
                dataFile[i].salary, 
                dataFile[i].id, 
                dataFile[i].type
            )
        } else if (dataFile[i].role == 'CEO') {
            employees[i] = new CEO(
                dataFile[i].firstname, 
                dataFile[i].lastname, 
                dataFile[i].salary, 
                dataFile[i].id, 
                dataFile[i].dressCode
            )
        } else if (dataFile[i].role == 'OfficeCleaner') {
            employees[i] = new OfficeCleaner(
                dataFile[i].firstname, 
                dataFile[i].lastname, 
                dataFile[i].salary, 
                dataFile[i].id, 
                dataFile[i].dressCode
            )
        };
        somchai.employeesRaw[i] = employees[i];
    };

    for (i = 0; i < somchai.employeesRaw.length; i++) {
        employees[i].talk('ID_' + somchai.employeesRaw[i].id + ' : My name is ' + somchai.employeesRaw[i].firstname + ' ' + somchai.employeesRaw[i].lastname + '. Class of : ' + somchai.employeesRaw[i].constructor.name);
        employees[i].work(somsri);
        console.log(' ');
    };
});