const fs = require('fs');
const os = require('os');
const EventEmitter = require('events');
const { CEO } = require('./CEO.js');

const myEmitter = new EventEmitter();

let robotData = '';

fs.readFile('head.txt', 'utf8', function(err, data) {
    robotData = data;
    fs.readFile('body.txt', 'utf8', function(err, data) {
        robotData = robotData + os.EOL + data;
        fs.readFile('leg.txt', 'utf8', function(err, data) {
            robotData = robotData + os.EOL + data;
            fs.readFile('feet.txt', 'utf8', function(err, data) {
                robotData = robotData + os.EOL + data;
                        
                fs.writeFileSync('robot.txt',robotData, 'utf8');

                myEmitter.emit('event_CEO',somchai,robotData);
            });
        });
    });
});

let somchai = new CEO('Somchai','Sudlor',30000);
myEmitter.on('event_CEO', somchai.reportRobot);

/* myEmitter.on('event_CEO', () => {
    let somchai = new CEO('Somchai','Sudlor',30000);
    somchai.reportRobot(somchai,robotData);
});
 */