const fs = require('fs');
const { Employee } = require('./Employee.js');

class OfficeCleaner extends Employee {
    constructor(firstname, lastname, salary, id, dressCode) {
        super(firstname, lastname, salary);
        this.id = id;
        this.dressCode = dressCode; //Normal, Maid
    };

    work() {  // simulate public method
        this._Clean();
        this._KillCoachroach();
        this._DecorateRoom();
        this._WelcomeGuest();
    };
    _Clean() { // simulate private method
        console.log('Clean');
    };
    _KillCoachroach() { // simulate private method
        console.log('KillCoachroach');
    };
    _DecorateRoom() { // simulate private method
        console.log('DecorateRoom');
    };
    _WelcomeGuest() { // simulate private method
        console.log('WelcomeGuest');
    };
          
    leaveForVacation(year, month, day) {
    };
    
    gossip(employee, message) {
    };

    talk(message) {
        console.log(message);
    };
}

module.exports = {
    OfficeCleaner
};