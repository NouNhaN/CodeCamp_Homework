const fs = require('fs');
const { Employee } = require('./Employee.js');

class CEO extends Employee {
    constructor(firstname, lastname, salary, id, dressCode) {
        super(firstname, lastname, salary);
        this.id = id;
        this.dressCode = dressCode;
        this.employeesRaw = [];

        let self = this;
    };

    work(employee) {  // simulate public method
        this._Fire(employee);
        this._Seminar();
        this._Golf();
        this._Hire(employee);
    };
    _Fire(employee) { // simulate private method
        this.dressCode = 'tshirt';
        console.log(employee.firstname + ' has been fired!' + ' Dress with :' + this.dressCode);
    };
    _Seminar() { // simulate private method
        this.dressCode = 'suit';
        console.log("He is going to seminar." + " Dress with :" + this.dressCode);
    };
    _Golf() { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);
    };
    _Hire(employee) { // simulate private method
        this.dressCode = 'tshirt';
        console.log(employee.firstname + ' has been hired back!' + ' Dress with :' + this.dressCode);
    };

    gossip(employee, message) {
        console.log('Hey ' + employee.firstname + ', ' + message);
    };

    getSalary() {  // simulate public method
        return super.getSalary() * 2;
    };

    increaseSalary(employee, newSalary) {
        if (employee.setSalary(newSalary) == false) {
            console.log(employee.firstname + '\'s salary is less than before!!');
        } else {
            console.log(employee.firstname + '\'s salary has been set to ' + newSalary);
        };
    };

    _readPromise_dataFile(Filename) {
        return new Promise((resolve, reject) => {
            fs.readFile(Filename, 'utf8', (err, dataFile) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(dataFile);
                };
            });
        });
    }

    async readFileData(Filename, callbackFunction) {
        try {
            const dataFile = await this._readPromise_dataFile(Filename);
            callbackFunction(null, JSON.parse(dataFile));
        } catch (error) {
            console.error(error, null);
        }
    }

    talk(message) {
        console.log(message);
    };

    reportRobot(self, robotMessage) {
        self.talk(robotMessage);
    };
}

module.exports = {
    CEO
};