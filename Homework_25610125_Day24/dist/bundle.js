/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "dist";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _lib = __webpack_require__(1);

var addbtn = document.getElementById("btnAdd");
addbtn.addEventListener("click", addItem);

function addItem() {
    var input_Id = document.getElementById("inputId");
    console.log(input_Id);
}

/*

let removebtn = document.getElementsByClassName("btnRemove")
let updatebtn = document.getElementsByClassName("btnUpdate")
let i = 0;
for (i = 0; i < removebtn.length; i++) {
    removebtn[i].addEventListener("click", removeItem);
    updatebtn[i].addEventListener("click", updateItem);
}
// console.log(addbtn)
// console.log(removebtn)

function addItem() {
    let dataId = document.getElementById("dataId").value
    let title = document.getElementById("dataTitle").value
    let salePrice = document.getElementById("dataSalePrice").value
    let promotionDate = document.getElementById("dataPromotionDate").value
    let image = document.getElementById("dataImage").value

    let itemslist = document.getElementById("itemslist")    
    
    let newitem = document.createElement('div')   
    newitem.className = "col-sm-6 col-md-3"
    newitem.setAttribute("id", "box_"+dataId);
    newitem.setAttribute("data-id", dataId);
    // <div class="col-sm-6 col-md-3" id="box_${dataId}" data-id="${dataId}">
    newitem.innerHTML = `   
        <div class="card items">
            <div class="card-body">
                <h5 id="title_${dataId}">${title}</h5>
                <img class="img-responsive card-image" id="image_${dataId}" src="./images/${image}">
                <div class="card-text" id="price_${dataId}">Price:${salePrice}$</div>
                <a href="#" class="btn btn-primary btnUpdate" data-id="${dataId}">Edit</a>
                <a href="#" class="btn btn-danger btnRemove" data-id="${dataId}">Remove</a>
            </div>
        </div>    
    `
    let aa = newitem.getElementsByClassName('btnRemove')
    
    itemslist.appendChild(newitem) 
    
    removebtn = document.getElementsByClassName("btnRemove")
    updatebtn = document.getElementsByClassName("btnUpdate")
    for (i = 0; i < removebtn.length; i++) {
        if (removebtn[i].getAttribute("data-id") === dataId) {
            removebtn[i].addEventListener("click", function() {            
                let removeItem = document.getElementById("box_"+dataId)           
                itemslist.removeChild(removeItem)      
            });
        }
        if (updatebtn[i].getAttribute("data-id") === dataId) {
            updatebtn[i].addEventListener("click", function() {
                document.getElementById("title_"+dataId).innerHTML = 'Veromos'
                document.getElementById("image_"+dataId).src = './images/Veromos.png'
                document.getElementById("price_"+dataId).innerHTML = 'Price:77$'
            });
        }        
    }

    document.getElementById("dataId").value = ''
    document.getElementById("dataTitle").value = ''
    document.getElementById("dataSalePrice").value = ''
    document.getElementById("dataPromotionDate").value = ''
    document.getElementById("dataImage").value = ''  
}

function updateItem() {
    let dataId = this.getAttribute("data-id");
    document.getElementById("title_"+dataId).innerHTML = 'Veromos'
    document.getElementById("image_"+dataId).src = './images/Veromos.png'
    document.getElementById("price_"+dataId).innerHTML = 'Price:77$'
}

function removeItem() {
    let dataId = this.getAttribute("data-id");
    let removeItem = document.getElementById("box_"+dataId)
    console.log(removeItem) 
    itemslist.removeChild(removeItem)      
}

*/

//example 4-1
/*
  domUtil.setText('app', 'Hello DOM!')
   let hello = () => console.log('Hello')
   hello()
*/
//example 4-2
/* let data = [
  {
    id: 1,
    text: 'List A'
  },
  {
    id: 2,
    text: 'List B'
  },
  {
    id: 3,
    text: 'List C'
  }
]
domUtil.addItems ('app', data)
*/

//example 4-3
// domUtil.editItem('2', 'B List')

//example 4-4
// domUtil.removeItem('3', 'menu')

//example 4-7
/* domUtil.setHTML('app', '<button id="btnOpen">Open</button>')
let btn = document.getElementById('btnOpen')
btn.addEventListener('click', bomUtil.openWindow)
*/
// example 4-8
// document.getElementById("app").innerHTML = "Screen Width: " + screen.width

// example 4-9
/* document.getElementById("app").innerHTML = ` 
The full URL of this page is:<br> ${window.location.href}
host:<br>  ${window.location.hostname}
pathname: <br> ${window.location.pathname}
protocol: <br> ${window.location.protocol}`
*/

// example 4-10
/* document.getElementById("app").innerHTML = ` 
<br> The appName of navigator is : ${window.navigator.appName}
<br> The codeName of navigator is : ${window.navigator.appCodeName}
<br> The platform of platform is : ${window.navigator.platform}`
*/

// example 4-11
/* window.alert("I am an alert box!")
let app = document.getElementById("app")
if (window.confirm("Press a button!")) {
  app.innerHTML = "You pressed OK!"
} else {
  app.innerHTML = "You pressed Cancel!"
}*/

/* setTimeout( () => alert('Hello'), 3000 )
let counter = 0
let timeId = setInterval( () => {
  counter++
  if ( counter > 2) {
    alert(counter)
    clearInterval(timeId)
  }
}, 3000) */

// document.cookie = "username=John Smith; expires=Thu, Wed, 24 Jan 2018 01:35:54 GMT; path=/";

/*
If you opening the modal by js use:
$('#myModal').modal({backdrop: 'static', keyboard: false})  

    <button data-target="#myModal" data-toggle="modal" data-backdrop="static" data-keyboard="false">
    Launch demo modal
</button>`
*/

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _domUtil = __webpack_require__(2);

Object.defineProperty(exports, 'domUtil', {
  enumerable: true,
  get: function get() {
    return _domUtil.domUtil;
  }
});

var _bomUtil = __webpack_require__(3);

Object.defineProperty(exports, 'bomUtil', {
  enumerable: true,
  get: function get() {
    return _bomUtil.bomUtil;
  }
});

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var domUtil = exports.domUtil = function () {
  function domUtil() {
    _classCallCheck(this, domUtil);
  }

  _createClass(domUtil, null, [{
    key: 'setText',
    value: function setText(id, text) {
      var elem = document.getElementById(id);
      elem.innerText = text;
    }
  }, {
    key: 'setHTML',
    value: function setHTML(id, html) {
      var elem = document.getElementById(id);
      elem.innerHTML = html;
    }
  }, {
    key: 'addItems',
    value: function addItems(id, data) {
      var parent = document.getElementById(id);
      var ul = document.createElement("ul");
      ul.id = 'menu';

      data.forEach(function (a) {
        console.log('a', a);
        var li = document.createElement("li");
        //example 4-5
        // li.id = a.id
        // li.style.color = 'blue'
        // li.style.cursor = 'pointer'
        //example 4-6
        // li.addEventListener("click", () => alert(a.text))
        li.appendChild(document.createTextNode(a.text));
        ul.appendChild(li);
      });
      parent.appendChild(ul);
    }
  }, {
    key: 'editItem',
    value: function editItem(id, text) {
      var elem = document.getElementById(id);
      elem.innerText = text;
    }
  }, {
    key: 'removeItem',
    value: function removeItem(removeId, parentId) {
      var elem = document.getElementById(removeId);
      var parent = document.getElementById(parentId);
      parent.removeChild(elem);
    }
  }]);

  return domUtil;
}();

/*
export class domUtil{
    static setText(id,text){
        let elem=document.getElementById(id)
        elem.innerText=text
    }
     getText(id){
         let elem = document.getElementById(id)
         return elem.innerText
     }
     static openWindow () {
        this.win = window.open("list.html", "",
        "resizable=yes,top=100,left=500,width=500,height=600")
        }
        static closeWindow () {
        if (this.win) {
        this.win.close()
        }
    }

}
*/

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var bomUtil = exports.bomUtil = function () {
  function bomUtil() {
    _classCallCheck(this, bomUtil);
  }

  _createClass(bomUtil, null, [{
    key: "openWindow",
    value: function openWindow() {
      this.win = window.open("https://www.w3schools.com", "", "width=200,height=100");
    }
  }, {
    key: "closeWindow",
    value: function closeWindow() {
      if (this.win) {
        this.win.close();
      }
    }
  }]);

  return bomUtil;
}();

/***/ })
/******/ ]);