import {
    domUtil,
    bomUtil
} from './lib';

let addbtn = document.getElementById("btnAdd")
addbtn.addEventListener("click", addItem);

function addItem() {
    let input_Id = document.getElementById("inputId")
    console.log(input_Id)
}









/*

let removebtn = document.getElementsByClassName("btnRemove")
let updatebtn = document.getElementsByClassName("btnUpdate")
let i = 0;
for (i = 0; i < removebtn.length; i++) {
    removebtn[i].addEventListener("click", removeItem);
    updatebtn[i].addEventListener("click", updateItem);
}
// console.log(addbtn)
// console.log(removebtn)

function addItem() {
    let dataId = document.getElementById("dataId").value
    let title = document.getElementById("dataTitle").value
    let salePrice = document.getElementById("dataSalePrice").value
    let promotionDate = document.getElementById("dataPromotionDate").value
    let image = document.getElementById("dataImage").value

    let itemslist = document.getElementById("itemslist")    
    
    let newitem = document.createElement('div')   
    newitem.className = "col-sm-6 col-md-3"
    newitem.setAttribute("id", "box_"+dataId);
    newitem.setAttribute("data-id", dataId);
    // <div class="col-sm-6 col-md-3" id="box_${dataId}" data-id="${dataId}">
    newitem.innerHTML = `   
        <div class="card items">
            <div class="card-body">
                <h5 id="title_${dataId}">${title}</h5>
                <img class="img-responsive card-image" id="image_${dataId}" src="./images/${image}">
                <div class="card-text" id="price_${dataId}">Price:${salePrice}$</div>
                <a href="#" class="btn btn-primary btnUpdate" data-id="${dataId}">Edit</a>
                <a href="#" class="btn btn-danger btnRemove" data-id="${dataId}">Remove</a>
            </div>
        </div>    
    `
    let aa = newitem.getElementsByClassName('btnRemove')
    
    itemslist.appendChild(newitem) 
    
    removebtn = document.getElementsByClassName("btnRemove")
    updatebtn = document.getElementsByClassName("btnUpdate")
    for (i = 0; i < removebtn.length; i++) {
        if (removebtn[i].getAttribute("data-id") === dataId) {
            removebtn[i].addEventListener("click", function() {            
                let removeItem = document.getElementById("box_"+dataId)           
                itemslist.removeChild(removeItem)      
            });
        }
        if (updatebtn[i].getAttribute("data-id") === dataId) {
            updatebtn[i].addEventListener("click", function() {
                document.getElementById("title_"+dataId).innerHTML = 'Veromos'
                document.getElementById("image_"+dataId).src = './images/Veromos.png'
                document.getElementById("price_"+dataId).innerHTML = 'Price:77$'
            });
        }        
    }

    document.getElementById("dataId").value = ''
    document.getElementById("dataTitle").value = ''
    document.getElementById("dataSalePrice").value = ''
    document.getElementById("dataPromotionDate").value = ''
    document.getElementById("dataImage").value = ''  
}

function updateItem() {
    let dataId = this.getAttribute("data-id");
    document.getElementById("title_"+dataId).innerHTML = 'Veromos'
    document.getElementById("image_"+dataId).src = './images/Veromos.png'
    document.getElementById("price_"+dataId).innerHTML = 'Price:77$'
}

function removeItem() {
    let dataId = this.getAttribute("data-id");
    let removeItem = document.getElementById("box_"+dataId)
    console.log(removeItem) 
    itemslist.removeChild(removeItem)      
}

*/

//example 4-1
/*
  domUtil.setText('app', 'Hello DOM!')
   let hello = () => console.log('Hello')
   hello()
*/
//example 4-2
/* let data = [
  {
    id: 1,
    text: 'List A'
  },
  {
    id: 2,
    text: 'List B'
  },
  {
    id: 3,
    text: 'List C'
  }
]
domUtil.addItems ('app', data)
*/

//example 4-3
// domUtil.editItem('2', 'B List')

//example 4-4
// domUtil.removeItem('3', 'menu')

//example 4-7
/* domUtil.setHTML('app', '<button id="btnOpen">Open</button>')
let btn = document.getElementById('btnOpen')
btn.addEventListener('click', bomUtil.openWindow)
*/
// example 4-8
// document.getElementById("app").innerHTML = "Screen Width: " + screen.width

// example 4-9
/* document.getElementById("app").innerHTML = ` 
The full URL of this page is:<br> ${window.location.href}
host:<br>  ${window.location.hostname}
pathname: <br> ${window.location.pathname}
protocol: <br> ${window.location.protocol}`
*/

// example 4-10
/* document.getElementById("app").innerHTML = ` 
<br> The appName of navigator is : ${window.navigator.appName}
<br> The codeName of navigator is : ${window.navigator.appCodeName}
<br> The platform of platform is : ${window.navigator.platform}`
*/

// example 4-11
/* window.alert("I am an alert box!")
let app = document.getElementById("app")
if (window.confirm("Press a button!")) {
  app.innerHTML = "You pressed OK!"
} else {
  app.innerHTML = "You pressed Cancel!"
}*/

/* setTimeout( () => alert('Hello'), 3000 )
let counter = 0
let timeId = setInterval( () => {
  counter++
  if ( counter > 2) {
    alert(counter)
    clearInterval(timeId)
  }
}, 3000) */

// document.cookie = "username=John Smith; expires=Thu, Wed, 24 Jan 2018 01:35:54 GMT; path=/";

/*
If you opening the modal by js use:
$('#myModal').modal({backdrop: 'static', keyboard: false})  

    <button data-target="#myModal" data-toggle="modal" data-backdrop="static" data-keyboard="false">
    Launch demo modal
</button>`
*/