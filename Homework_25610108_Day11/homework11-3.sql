﻿DELETE FROM book;
DELETE FROM booksale;

เพิ่มหนังสือเข้าไป 10 เล่ม
	- 	INSERT INTO book (ISBN , bookname, bookprice)
		VALUE
		('ISBN_0001', 'JavaScript',100),
		('ISBN_0002', 'bookname_02',101),
		('ISBN_0003', 'bookname_03',102),
		('ISBN_0004', 'bookname_04',103),
		('ISBN_0005', 'bookname_05',104),
		('ISBN_0006', 'bookname_06',105),
		('ISBN_0007', 'bookname_07',106),
		('ISBN_0008', 'bookname_08',107),
		('ISBN_0009', 'bookname_09',108),
		('ISBN_0010', 'bookname_10',109);

ค้นหาหนังสือจากชื่อ เช่น JavaScript ให้ค้นว่า Script ก็ต้องหาเจอ
	-	SELECT ISBN, bookname, bookprice FROM book WHERE bookname LIKE '%Script%';

ดึงหนังสือมาแสดง 4 เล่มแรก ที่ชื่อมีตัวอักษร a
	- 	SELECT ISBN, bookname, bookprice FROM book 
		WHERE bookname LIKE '%a%'
		ORDER BY ISBN
		limit 4 offset 0;

เพิ่มรายการขายหนังสือ 20 รายการ
	- 	INSERT INTO booksale (ISBN , id, bookprice, booknum, date_sale)
		VALUE
		('ISBN_0001', 1, 200, 2, '25610107'),
		('ISBN_0003', 1, 206, 3, '25610107'),
		('ISBN_0005', 1, 520, 5, '25610107'),
		('ISBN_0007', 1, 106, 1, '25610107'),
		('ISBN_0010', 1, 109, 1, '25610107'),
		('ISBN_0001', 3, 100, 1, '25610107'),
		('ISBN_0001', 7, 300, 3, '25610107'),
		('ISBN_0001', 8, 400, 4, '25610107'),
		('ISBN_0001', 9, 100, 1, '25610107'),
		('ISBN_0004', 2, 103, 1, '25610107'),
		('ISBN_0005', 4, 104, 1, '25610108'),
		('ISBN_0006', 6, 105, 1, '25610108'),
		('ISBN_0007', 7, 106, 1, '25610108'),
		('ISBN_0008', 7, 107, 1, '25610108'),
		('ISBN_0009', 8, 108, 1, '25610108'),
		('ISBN_0005', 1, 104, 1, '25610108'),
		('ISBN_0006', 2, 105, 1, '25610108'),
		('ISBN_0007', 3, 106, 1, '25610108'),
		('ISBN_0008', 9, 107, 1, '25610108'),
		('ISBN_0009', 2, 108, 1, '25610109');

หาว่าขายหนังสือได้กี่เล่ม
	-	SELECT SUM(booknum) FROM booksale;

หาว่ามีหนังสืออะไรบ้างที่ขายออก (แสดงแค่ ISBN ของหนังสือ)
	-	SELECT DISTINCT(ISBN) FROM booksale order by ISBN;

หาว่าขายหนังสือได้เงินทั้งหมดเท่าไร
	
	-	SELECT SUM(bookprice) FROM booksale;
	
			
