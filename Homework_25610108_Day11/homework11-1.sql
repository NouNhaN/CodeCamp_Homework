﻿DROP DATABASE BookStore;

สร้าง database สำหรับร้านหนังสือ
	-	CREATE DATABASE BookStore;

สร้าง table ดังนี้
	พนักงาน (id, ชื่อ, นามสกุล, อายุ, วันที่เวลาที่เพิ่มข้อมูล)
		- 	CREATE TABLE employee (
				id int auto_increment,
				firstname varchar(50),
				lastname varchar(50),
				age int default 0,
				date_created timestamp default now(),
				primary key (id)
				);

	หนังสือ (ISBN, ชื่อหนังสือ, ราคา, วันเวลาที่เพิ่มข้อมูล)
		- 	CREATE TABLE book (
				ISBN varchar(50),
				bookname varchar(255),
				bookprice float default 0,
				date_created timestamp default now(),
				primary key (ISBN),
				unique (ISBN)
				);

	รายการขาย (ISBN หนังสือ, พนักงานที่ขาย, ราคาที่ขาย, จำนวนเล่ม, วันเวลาที่ขาย
		- 	CREATE TABLE booksale (
				ISBN varchar(50),
				id int default 0,
				bookprice float default 0,
				booknum int default 0,
				date_sale varchar(8)
				);
				


				

