หาชื่อ database ที่ import
        - show databases;
            +--------------------+
            | Database           |
            +--------------------+
            | bookstore          |
            | codecamp           |
            | db1                |
            | employees          |
            | information_schema |
            | mysql              |
            | performance_schema |
            | phpmyadmin         |
            | test               |
            +--------------------+

หาชื่อ tables ทั้งหมดที่ import
        - show tables;
            +----------------------+
            | Tables_in_employees  |
            +----------------------+
            | current_dept_emp     |
            | departments          |
            | dept_emp             |
            | dept_emp_latest_date |
            | dept_manager         |
            | employees            |
            | salaries             |
            | titles               |
            +----------------------+

ดูโครงสร้างว่าแต่ละ table มี column อะไรบ้าง
        - describe current_dept_emp;
            +-----------+---------+------+-----+---------+-------+
            | Field     | Type    | Null | Key | Default | Extra |
            +-----------+---------+------+-----+---------+-------+
            | emp_no    | int(11) | NO   |     | NULL    |       |
            | dept_no   | char(4) | NO   |     | NULL    |       |
            | from_date | date    | YES  |     | NULL    |       |
            | to_date   | date    | YES  |     | NULL    |       |
            +-----------+---------+------+-----+---------+-------+
        - describe departments;
            +-----------+-------------+------+-----+---------+-------+
            | Field     | Type        | Null | Key | Default | Extra |
            +-----------+-------------+------+-----+---------+-------+
            | dept_no   | char(4)     | NO   | PRI | NULL    |       |
            | dept_name | varchar(40) | NO   | UNI | NULL    |       |
            +-----------+-------------+------+-----+---------+-------+            
        - describe dept_emp;
            +-----------+---------+------+-----+---------+-------+
            | Field     | Type    | Null | Key | Default | Extra |
            +-----------+---------+------+-----+---------+-------+
            | emp_no    | int(11) | NO   | PRI | NULL    |       |
            | dept_no   | char(4) | NO   | PRI | NULL    |       |
            | from_date | date    | NO   |     | NULL    |       |
            | to_date   | date    | NO   |     | NULL    |       |
            +-----------+---------+------+-----+---------+-------+
        - describe dept_emp_latest_date;
            +-----------+---------+------+-----+---------+-------+
            | Field     | Type    | Null | Key | Default | Extra |
            +-----------+---------+------+-----+---------+-------+
            | emp_no    | int(11) | NO   |     | NULL    |       |
            | from_date | date    | YES  |     | NULL    |       |
            | to_date   | date    | YES  |     | NULL    |       |
            +-----------+---------+------+-----+---------+-------+
        - describe dept_manager;
            +-----------+---------+------+-----+---------+-------+
            | Field     | Type    | Null | Key | Default | Extra |
            +-----------+---------+------+-----+---------+-------+
            | emp_no    | int(11) | NO   | PRI | NULL    |       |
            | dept_no   | char(4) | NO   | PRI | NULL    |       |
            | from_date | date    | NO   |     | NULL    |       |
            | to_date   | date    | NO   |     | NULL    |       |
            +-----------+---------+------+-----+---------+-------+
        - describe employees;
            +------------+---------------+------+-----+---------+-------+
            | Field      | Type          | Null | Key | Default | Extra |
            +------------+---------------+------+-----+---------+-------+
            | emp_no     | int(11)       | NO   | PRI | NULL    |       |
            | birth_date | date          | NO   |     | NULL    |       |
            | first_name | varchar(14)   | NO   |     | NULL    |       |
            | last_name  | varchar(16)   | NO   |     | NULL    |       |
            | gender     | enum('M','F') | NO   |     | NULL    |       |
            | hire_date  | date          | NO   |     | NULL    |       |
            +------------+---------------+------+-----+---------+-------+
        - describe salaries;
            +-----------+---------+------+-----+---------+-------+
            | Field     | Type    | Null | Key | Default | Extra |
            +-----------+---------+------+-----+---------+-------+
            | emp_no    | int(11) | NO   | PRI | NULL    |       |
            | salary    | int(11) | NO   |     | NULL    |       |
            | from_date | date    | NO   | PRI | NULL    |       |
            | to_date   | date    | NO   |     | NULL    |       |
            +-----------+---------+------+-----+---------+-------+
        - describe titles;        
            +-----------+-------------+------+-----+---------+-------+
            | Field     | Type        | Null | Key | Default | Extra |
            +-----------+-------------+------+-----+---------+-------+
            | emp_no    | int(11)     | NO   | PRI | NULL    |       |
            | title     | varchar(50) | NO   | PRI | NULL    |       |
            | from_date | date        | NO   | PRI | NULL    |       |
            | to_date   | date        | YES  |     | NULL    |       |
            +-----------+-------------+------+-----+---------+-------+       

บริษัทนี้มีพนักงานตำแหน่งอะไรบ้าง
        - SELECT DISTINCT(title) FROM titles ORDER BY title;
            +--------------------+
            | title              |
            +--------------------+
            | Assistant Engineer |
            | Engineer           |
            | Manager            |
            | Senior Engineer    |
            | Senior Staff       |
            | Staff              |
            | Technique Leader   |
            +--------------------+

บริษัทนี้มีพนักงานกี่คน เป็นผู้ชายกี่คน ผู้หญิงกี่คน
        -   SELECT COUNT(*) AS emp_count
            , SUM(IF(gender = 'M',1,0)) AS emp_male
            , SUM(IF(gender = 'F',1,0)) AS emp_female  
            FROM employees;
            +-----------+----------+------------+
            | emp_count | emp_male | emp_female |
            +-----------+----------+------------+
            |    300024 |   179973 |     120051 |
            +-----------+----------+------------+

บริษัทนี้มีพนักงานที่นามสกุลต่างกันกี่คน
        -   SELECT COUNT(DISTINCT(last_name)) AS last_name_count FROM employees;
            +-----------------+
            | last_name_count |
            +-----------------+
            |            1637 |
            +-----------------+