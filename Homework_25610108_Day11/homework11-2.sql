﻿DELETE FROM employee;

เพิ่มพนักงานเข้าไป 10 คน โดยที่ข้อมูลห้ามซ้ำกัน
	- 	INSERT INTO employee (id, firstname, lastname, age)
		VALUE
		(1, 'FirstName_01', 'LastName_01',21),
		(2, 'FirstName_02', 'LastName_02',22),
		(3, 'FirstName_03', 'LastName_03',23),
		(4, 'FirstName_04', 'LastName_04',24),
		(5, 'FirstName_05', 'LastName_05',25),
		(6, 'FirstName_06', 'LastName_06',26),
		(7, 'FirstName_07', 'LastName_07',27),
		(8, 'FirstName_08', 'LastName_08',28),
		(9, 'FirstName_09', 'LastName_09',29),
		(10, 'FirstName_10', 'LastName_10',30);

พนักงานคนที่ 5 ลาออก ลบข้อมูลคนที่ 5 ออก
	-	DELETE FROM employee 
		WHERE id = 5;

เจ้าของร้านอยากเก็บที่อยู่พนักงานเพิ่ม ใส่ที่อยู่ให้พนักงานแต่ละคน คนไหนไม่รู้ให้เป็น NULL
	-	ALTER TABLE employee
		ADD COLUMN address varchar(255);
		
	-	UPDATE employee SET address = 'Address_01' WHERE id = 1;
	-	UPDATE employee SET address = 'Address_02' WHERE id = 2;
	-	UPDATE employee SET address = 'Address_03' WHERE id = 3;
	-	UPDATE employee SET address = 'Address_04' WHERE id = 4;
	-	UPDATE employee SET address = NULL WHERE id = 5;
	-	UPDATE employee SET address = NULL WHERE id = 6;
	-	UPDATE employee SET address = 'Address_07' WHERE id = 7;
	-	UPDATE employee SET address = 'Address_08' WHERE id = 8;
	-	UPDATE employee SET address = 'Address_09' WHERE id = 9;
	-	UPDATE employee SET address = 'Address_10' WHERE id = 10;

หาจำนวนพนักงานทั้งหมดในร้าน
	-	SELECT COUNT(*) FROM employee;

แสดงรายชื่อพนักงานที่อายุน้อยกว่า 20 ปี
	- 	SELECT id, firstname, lastname, age, address FROM employee
		WHERE age < 20
		limit 100 offset 0;
	
				

