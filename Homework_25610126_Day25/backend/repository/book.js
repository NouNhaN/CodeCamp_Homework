class Book {
  constructor() {
    this.isbn;
    this.book_name;
    this.price;
    this.create_at;
    this.pic;
    this.promotion_date;
  }

  createEntity(row) {
    return {
      isbn: row.isbn,
      book_name: row.book_name,
      price: row.price,
      created_at: row.created_at,
      pic: row.pic,
      promotion_date: row.promotion_date
    };
  }
}

module.exports.find = async function(db, isbn) {
  const [rows] = await db.execute(
    `SELECT isbn, book_name, price, pic, promotion_date, created_at 
    FROM book 
    WHERE isbn = ?`,
    [isbn]
  );
  return new Book().createEntity(rows[0]);
};
module.exports.findAll = async function(db) {
  const [rows] = await db.execute(
    `SELECT isbn, book_name, price, pic, promotion_date, created_at 
    FROM book`
  );
  return rows.map(row => new Book().createEntity(row));
};
module.exports.store = async function(db, book) {
  await db.beginTransaction();
  const result = await db.execute(
    `INSERT INTO book (
      isbn, book_name, price, pic, promotion_date
    ) VALUES (
      ?, ?, ?, ?, ?
    )`,
    [book.isbn, book.book_name, book.price, book.pic, book.promotion_date]
  );
  await db.commit();
  return book.isbn;
};
module.exports.update = async function(db, book) {
  return db.execute(
    `UPDATE book
    SET book_name = ?, price = ?, image = ?, promotion_date = ? 
    WHERE isbn = ?`,
    [book.book_name, book.price, book.pic, book.promotion_date, book.isbn]
  );
};
module.exports.remove = async function(db, isbn) {
  return db.execute(
    `DELETE FROM book 
    WHERE isbn = ?`, 
    [isbn]
  );
};
