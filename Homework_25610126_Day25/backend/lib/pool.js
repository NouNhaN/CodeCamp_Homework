const mysql = require("mysql2/promise");
const dbConfig = require("../config/database.js");

const pool = mysql.createPool({
  host: dbConfig.host,
  user: dbConfig.user,
  password: dbConfig.password,
  port: dbConfig.port,
  database: dbConfig.database
});

module.exports.getConnection = async function() {
  return pool.getConnection();
};
