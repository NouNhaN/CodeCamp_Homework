class Database {
  constructor() {
    this.connection = this.myConnection();
  }
  async myConnection() {
    const mysql = require("mysql2/promise");
    const myConfig = require("../config/database.js");
    const myconnection = await mysql.createPool({
      connectionLimit: myConfig.maxconnection,
      host: myConfig.host,
      user: myConfig.user,
      password: myConfig.password,
      database: myConfig.database
    });

    return myconnection;
  }
  async execute(conn, sql) {
    let rows = await conn.execute(sql);
    return rows;
  }
}

module.exports.db = new Database();