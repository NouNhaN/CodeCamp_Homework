const Koa = require("koa");
const render = require("koa-ejs");
const cors = require("koa2-cors");
const bodyParser = require("koa-bodyparser");

const app = new Koa();

app.use( async (ctx, next) => {
  ctx.body = 'homework_day25';
  await next();
})

app.use(
  cors({
    origin: function(ctx) {
      if (ctx.url === "/test") {
        return false;
      }
      return "*";
    },
    exposeHeaders: ["WWW-Authenticate", "Server-Authorization"],
    maxAge: 5,
    credentials: true,
    allowMethods: ["GET", "POST", "DELETE"],
    allowHeaders: ["Content-Type", "Authorization", "Accept"]
  })
);

app.use(bodyParser());

const router = require("./routes/bookRoute")(app);
app.use(router);

app.listen(3000);
