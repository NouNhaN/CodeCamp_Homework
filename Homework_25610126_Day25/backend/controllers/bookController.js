const bookList = require("../repository/book");
const pool = require("../lib/pool");

exports.list = async function(ctx, next) {
  const db = await pool.getConnection();
  try {
    let books = await bookList.findAll(db);
    ctx.body = books;
    await next();
  } catch (err) {
    ctx.status = 400;
    ctx.body = err.message;
  } finally {
    db.release();
  }
};

exports.create = async function(ctx, next) {
  const db = await pool.getConnection();
  try {
    bookList.isbn = ctx.request.body.isbn;
    bookList.book_name = ctx.request.body.book_name;
    bookList.price = ctx.request.body.price;
    bookList.pic = ctx.request.body.pic;
    bookList.promotion_date = ctx.request.body.promotion_date;
    await bookList.store(db, bookList);
    ctx.body = {
      success: true,
      data: {
        isbn: ctx.request.body.isbn,
        book_name: ctx.request.body.book_name,
        price: ctx.request.body.price,
        pic: ctx.request.body.pic,
        promotion_date: ctx.request.body.promotion_date
      }
    };
    await next();
  } catch (err) {
    ctx.status = 400;
    ctx.body = err.message;
  } finally {
    db.release();
  }
};

exports.get = async function(ctx, next) {
  const db = await pool.getConnection();
  try {
    const books = await bookList.find(db, ctx.params.id);
    ctx.body = books;
    await next();
  } catch (err) {
    ctx.status = 400;
    ctx.body = err.message;
  } finally {
    db.release();
  }
};

exports.update = async function(ctx, next) {
  const db = await pool.getConnection();
  try {
    const book = await bookList.find(db, ctx.params.id);
    book.book_name = ctx.request.body.book_name;
    book.price = ctx.request.body.price;
    book.pic = ctx.request.body.pic;
    book.promotion_date = ctx.request.body.promotion_date;
    await bookList.update(db, book);
    ctx.body = {
      success: true,
      data: {
        isbn: ctx.request.body.isbn,
        book_name: ctx.request.body.book_name,
        price: ctx.request.body.price,
        pic: ctx.request.body.pic,
        promotion_date: ctx.request.body.promotion_date
      }
    };
    await next();
  } catch (err) {
    ctx.status = 400;
    ctx.body = err.message;
  } finally {
    db.release();
  }
};

exports.remove = async function(ctx, next) {
  const db = await pool.getConnection();
  try {
    await bookList.remove(db, ctx.params.id);
    ctx.body = {
      success: true
    };
    await next();
  } catch (err) {
    ctx.status = 400;
    ctx.body = err.message;
  } finally {
    db.release();
  }
};
