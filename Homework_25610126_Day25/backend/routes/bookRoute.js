const Router = require("koa-router");

module.exports = app => {
  let bookList = require("../controllers/bookController");
  const router = new Router()
    .get("/book", bookList.list)
    .post("/book", bookList.create)
    .get("/book/:id", bookList.get)
    .post("/book/:id", bookList.update)
    .delete("/book/:id", bookList.remove);

  return router.routes();
};
