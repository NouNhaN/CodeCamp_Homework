import { domutil as dom } from "./libs";

let addbtn = document.getElementById("btnAdd");
addbtn.addEventListener("click", addItem);

$("#myModalNorm").on("hidden.bs.modal", function() {
  document.getElementById("myModalLabel").innerHTML = "Add Data";
  document.getElementById("dataId").value = "";
  document.getElementById("dataTitle").value = "";
  document.getElementById("dataSalePrice").value = "";
  document.getElementById("dataPromotionDate").value = "";
  document.getElementById("dataImage").value = "";
});

getDataList();

function getDataList() {
  let url = "http://localhost:3000/book";
  fetch(url, {
    method: "GET", // or 'PUT'
    headers: new Headers({
      "Content-Type": "application/json"
    })
  })
    .then(res => res.json())
    .catch(error => console.error("Error:", error))
    .then(response => {
      console.log("Success:", response);
      response.forEach(element => {
        // console.log(element.isbn)
        let newitem = document.createElement("div");
        newitem.className = "col-sm-6 col-md-3";
        newitem.setAttribute("id", "box_" + element.isbn);
        newitem.setAttribute("data-id", element.isbn);
        // newitem.innerHTML = `   
        //             <div class="card items">
        //                 <div class="card-body">
        //                     <h5 id="title_${element.isbn}">${element.book_name}</h5>
        //                     <img class="img-responsive card-image" id="image_${element.isbn}" src="${element.pic}">
        //                     <div class="card-text" id="price_${element.isbn}">Price:${element.price}</div>
        //                     <div class="card-text" id="promo_${element.isbn}">${element.promotion_date}</div>
        //                     <a href="#" class="btn btn-primary btnEdit" data-id="${element.isbn}" data-toggle="modal" data-target="#myModalNorm">Edit</a>
        //                     <a href="#" class="btn btn-danger btnRemove" data-id="${element.isbn}" data-toggle="confirmation" data-title="Remove Data?">Remove</a>
        //                 </div>
        //             </div>    
        //         `;
        newitem.innerHTML = `   
                <div class="card items">
                    <div class="card-body">
                        <h5 id="title_${element.isbn}">${element.book_name}</h5>
                        <img class="img-responsive card-image" id="image_${element.isbn}" src="${element.pic}">
                        <div class="card-text" id="price_${element.isbn}">Price:${element.price}</div>
                        <div class="card-text" id="promo_${element.isbn}">${element.promotion_date}</div>
                    </div>
                </div>    
            `;
    itemslist.appendChild(newitem);
      });

      let editbtn = document.getElementsByClassName("btnEdit");
      let i = 0;
      for (i = 0; i < editbtn.length; i++) {
        editbtn[i].addEventListener("click", editItem);
      }

      $("[data-toggle=confirmation]").confirmation({
        rootSelector: "[data-toggle=confirmation]",
        container: "body",
        title: "Remove Content ?",
        onConfirm: function() {
          let dataId = $(this).data("id");
          let remove = document.getElementById("box_" + dataId);
          let url = "http://localhost:3000/book/" + dataId;
          fetch(url, {
            method: "DELETE", // or 'PUT'
            headers: new Headers({
              "Content-Type": "application/json"
            })
          })
            .then(res => res.json())
            .catch(error => console.error("Error:", error))
            .then(response => {
              console.log("Success:", response);
              itemslist.removeChild(remove);
            });
        }
      });
    });
}

function addItem() {
  let dataId = document.getElementById("dataId").value;
  let title = document.getElementById("dataTitle").value;
  let salePrice = document.getElementById("dataSalePrice").value;
  let promotionDate = document.getElementById("dataPromotionDate").value;
  let image = document.getElementById("dataImage").value;
  let itemslist = document.getElementById("itemslist");

  if (document.getElementById("myModalLabel").innerHTML === "Add Data") {
    let url = "http://localhost:3000/book";
    var data = {
      isbn: dataId,
      book_name: title,
      price: salePrice,
      promotion_date: promotionDate,
      pic: image
    };
    fetch(url, {
      method: "POST", // or 'PUT'
      body: JSON.stringify(data),
      headers: new Headers({
        "Content-Type": "application/json"
      })
    })
      .then(res => res.json())
      .catch(error => console.error("Error:", error))
      .then(response => {
        console.log("Success:", response);

        let newitem = document.createElement("div");
        newitem.className = "col-sm-6 col-md-3";
        newitem.setAttribute("id", "box_" + response.data.isbn);
        newitem.setAttribute("data-id", response.data.isbn);
        // newitem.innerHTML = `   
        //             <div class="card items">
        //                 <div class="card-body">
        //                     <h5 id="title_${response.data.isbn}">${response.data.book_name}</h5>
        //                     <img class="img-responsive card-image" id="image_${response.data.isbn}" src="${response.data.pic}">
        //                     <div class="card-text" id="price_${response.data.isbn}">Price:${response.data.price}</div>
        //                     <div class="card-text" id="promo_${response.data.isbn}">${response.data.promotion_date}</div>
        //                     <a href="#" class="btn btn-primary btnEdit" data-id="${response.data.isbn}" data-toggle="modal" data-target="#myModalNorm">Edit</a>
        //                     <a href="#" class="btn btn-danger btnRemove" data-id="${response.data.isbn}" data-toggle="confirmation" data-title="Remove Data?">Remove</a>
        //                 </div>
        //             </div>    
        //         `;
        newitem.innerHTML = `   
                <div class="card items">
                    <div class="card-body">
                        <h5 id="title_${response.data.isbn}">${response.data.book_name}</h5>
                        <img class="img-responsive card-image" id="image_${response.data.isbn}" src="${response.data.pic}">
                        <div class="card-text" id="price_${response.data.isbn}">Price:${response.data.price}</div>
                        <div class="card-text" id="promo_${response.data.isbn}">${response.data.promotion_date}</div>
                    </div>
                </div>    
            `;
    itemslist.appendChild(newitem);
        let removebtn = document.getElementsByClassName("btnRemove");
        let editbtn = document.getElementsByClassName("btnEdit");
        let i = 0;
        for (i = 0; i < editbtn.length; i++) {
          if (editbtn[i].getAttribute("data-id") === dataId) {
            $("[data-toggle=confirmation]").confirmation({
              rootSelector: "[data-toggle=confirmation]",
              container: "body",
              title: "Remove Content ?",
              onConfirm: function() {
                let dataId = $(this).data("id");
                let remove = document.getElementById("box_" + dataId);
                let url = "http://localhost:3000/book/" + dataId;
                fetch(url, {
                  method: "DELETE", // or 'PUT'
                  headers: new Headers({
                    "Content-Type": "application/json"
                  })
                })
                  .then(res => res.json())
                  .catch(error => console.error("Error:", error))
                  .then(response => {
                    console.log("Success:", response);
                    itemslist.removeChild(remove);
                  });
              }
            });
          }
          if (editbtn[i].getAttribute("data-id") === dataId) {
            editbtn[i].addEventListener("click", function() {
              document.getElementById("myModalLabel").innerHTML = "Edit Data";
              document.getElementById("dataId").value = dataId;
              document.getElementById(
                "dataTitle"
              ).value = document.getElementById("title_" + dataId).innerHTML;
              let priceVal = document
                .getElementById("price_" + dataId)
                .innerHTML.split(":");
              document.getElementById("dataSalePrice").value = priceVal[1];
              document.getElementById(
                "dataPromotionDate"
              ).value = document.getElementById("promo_" + dataId).innerHTML;
              document.getElementById(
                "dataImage"
              ).value = document.getElementById("image_" + dataId).src;
            });
          }
        }
      });
  } else if (
    document.getElementById("myModalLabel").innerHTML === "Edit Data"
  ) {
    let url = "http://localhost:3000/book/" + dataId;
    var data = {
      isbn: dataId,
      book_name: title,
      price: salePrice,
      promotion_date: promotionDate,
      //pic: document.getElementById("image_"+dataId).src,
      pic: image
    };
    fetch(url, {
      method: "POST", // or 'PUT'
      body: JSON.stringify(data),
      headers: new Headers({
        "Content-Type": "application/json"
      })
    })
      .then(res => res.json())
      .catch(error => console.error("Error:", error))
      .then(response => {
        console.log("Success:", response.data);
        document.getElementById("title_" + response.data.isbn).innerHTML =
          response.data.book_name;
        document.getElementById("price_" + response.data.isbn).innerHTML =
          "Price:" + response.data.price;
        document.getElementById("promo_" + response.data.isbn).innerHTML =
          response.data.promotion_date;
        document.getElementById("image_" + response.data.isbn).src =
          response.data.pic;
      });
    document.getElementById("myModalLabel").innerHTML = "Add Data";
  }
}

function editItem() {
  let dataId = this.getAttribute("data-id");

  document.getElementById("myModalLabel").innerHTML = "Edit Data";
  document.getElementById("dataId").value = dataId;
  document.getElementById("dataTitle").value = document.getElementById(
    "title_" + dataId
  ).innerHTML;
  let priceVal = document
    .getElementById("price_" + dataId)
    .innerHTML.split(":");
  document.getElementById("dataSalePrice").value = priceVal[1];
  document.getElementById("dataPromotionDate").value = document.getElementById(
    "promo_" + dataId
  ).innerHTML;
  document.getElementById("dataImage").value = document.getElementById(
    "image_" + dataId
  ).src;
}

function fetchData(url, method, data) {
  //let url = 'http://localhost:3000/book';
  // var data = {username: 'example'};
  fetch(url, {
    method: method, // or 'PUT'
    //body: JSON.stringify(data),
    headers: new Headers({
      "Content-Type": "application/json"
    })
  })
    .then(res => res.json())
    .catch(error => console.error("Error:", error))
    .then(response => console.log("Success:", response));
}
