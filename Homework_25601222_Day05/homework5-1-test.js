/*
• ตรวจสอบวา่ มีไฟล์ .json สร้างข้ึนมาหรือยงั
• ต้องเขียน Test ก่อนเสมอโดยเริ่มจากเขียนทดสอบไฟล์ .json ที่เป็ น output วา่ มีโครงสร้างถูกตอ้ งก่อนที่จะเขียน
โปรแกรมอ่านไฟลต์ น้ ทางที่จะมา write ไฟล์ .json จริง
• ตรวจสอบวา่ ไฟล์ .json น้นั มีโครงสร้างถูกตอ้ งตามโจทยห์ รือไม่ มีชื่อ key ตรงกนั หรือไม่ หากมีการแกไ้ ขไฟล์
.json ดว้ ยโปรแกรมขา้ งนอก ทาใหโ้ ครงสร้างเพ้ียนไป ตอ้ งแจง้ เตือนวา่ ไฟลม์ ีความผดิ ปกติ
• ในส่วนของการบา้ นขอ้ ยอ่ ยที่ 3 เรื่องจ านวนเพื่อน array ที่เป็ นผลลัพธ์ต้องมีขนาดของ array เท่ากบั จานวนคน
ท้งั หมดของไฟลต์ น้ ทาง จงเขียน test เพื่อทดสอบขนาด array น้ีในไฟล์ .json ด้วย
• เขียน test ทดสอบวา่ จานวนสีนยั นต์ าและจานวนเพศ เมื่อบวกรวมกนั ทุก key แลว้ จานวนเกินจานวนคนท้งั หมด
หรือไม่
*/

const fs = require('fs');
const assert = require('assert');
const { readPromise_dataFile,
        read_dataFile,
        writePromise_dataFile,
        create_Data_homework5_1 } = require('./homework5-1');

describe('tdd lab', () => {
    //ตรวจสอบว่าอ่านไฟล์ .json
    describe('#checkReadFile() - ( Async )', () => {
        it('( Async Promise ) - should File -- homework5.json -- not be empty', () => {
            return readPromise_dataFile('./homework5.json')
                .then((dataFile) => {
                    assert.strictEqual(dataFile.length > 0, true, 'homework5.json is empty');
                });
        });
        it('( Async not Promise ) - should File -- homework5.json -- not be empty', (done) => {
            read_dataFile('./homework5.json', (err, dataFile) => {
                if (err)
                    done(err);
                else {
                    assert.strictEqual(dataFile.length > 0, true, 'homework5.json is empty');
                    done();
                }
            });
        });
    });

    //ตรวจสอบว่ามีไฟล์ .json
     describe('#checkFileExist()', () => {
        it('should have homework5-1_eyes.json existed', async () => {
            const dataFile = await create_Data_homework5_1('1');
            assert.deepEqual(fs.existsSync('homework5-1_eyes.json'), true, 'homework5-1_eyes.json not existed');
        });
        it('should have homework5-1_gender.json existed', async () => {
            const dataFile = await create_Data_homework5_1('2');
            assert.deepEqual(fs.existsSync('homework5-1_gender.json'), true, 'homework5-1_gender.json not existed');
        }); 
        it('should have homework5-1_friends.json existed', async () => {
            const dataFile = await create_Data_homework5_1('3');
            assert.deepEqual(fs.existsSync('homework5-1_friends.json'), true, 'homework5-1_friends.json not existed');
        });
    });

    describe('#objectKey()', () => {
        it('should have same object key stucture as homework5-1_eyes.json', async () => {
            const dataFile = await create_Data_homework5_1('1');
            const dataFile_eyes = JSON.parse(dataFile);
            const data_eyes = {"brown": 0, "blue": 0, "green": 0}

            assert.deepEqual(Object.keys(dataFile_eyes), Object.keys(data_eyes), "object key stucture as homework5-1_eyes.json is not same");
        });
        it('should have same object key stucture as homework5-1_gender.json', async () => {
            const dataFile = await create_Data_homework5_1('2');
            const dataFile_gender = JSON.parse(dataFile);
            const data_gender = {"female": 0, "male": 0};

            assert.deepEqual(Object.keys(dataFile_gender), Object.keys(data_gender), "object key stucture as homework5-1_gender.json is not same");
        });
        it('should have same object key stucture as homework5-1_friends.json', async () => {
            const dataFile = await create_Data_homework5_1('3');
            const dataFile_friends = JSON.parse(dataFile);
            const data_friends = {"_id": '', "friendCount": 0};

            assert.deepEqual(Object.keys(dataFile_friends[0]), Object.keys(data_friends), "object key stucture as homework5-1_friends.json is not same");111
        });
    });

    describe('#userFriendCount()', () => {
        it('should have size of array input as 23', async () => {
            const dataFile = await create_Data_homework5_1('3');
            const dataFile_friends = JSON.parse(dataFile);

            assert.deepEqual(dataFile_friends.length, 23,'size of array input as 23');
        });
    });

    describe('#sumOfEyes()', () => {
        it('should have sum of eyes as 23', async () => {
            const dataFile = await create_Data_homework5_1('1');
            const dataFile_eyes = JSON.parse(dataFile);

            let sum_dataFile_eyes = 0;
            for (let x in dataFile_eyes) {
                sum_dataFile_eyes = sum_dataFile_eyes + dataFile_eyes[x];    
            }
            assert.deepEqual(sum_dataFile_eyes, 23,'sum of eyes input as 23');
        });
    });

    describe('#sumOfGender()', () => {
        it('should have sum of gender as 23', async () => {
            const dataFile = await create_Data_homework5_1('2');
            const dataFile_gender = JSON.parse(dataFile);

            let sum_dataFile_gender = 0;
            for (let x in dataFile_gender) {
                sum_dataFile_gender = sum_dataFile_gender + dataFile_gender[x];    
            }
            assert.deepEqual(sum_dataFile_gender, 23,'sum of gender input as 23');
        });
    });
});