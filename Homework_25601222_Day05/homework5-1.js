/*
• จงเข้า Download file
https://drive.google.com/open?id=13ejCslsXKkLx9XUTMRmxqOhMGOtNlcHS
• ใช้ JSON.Parse() อ่านค่าจาก file มาเกบ็ ค่าไว้
• จงเขียน function คานวณสิ่งเหล่าน้ีแยกกนั
1. นบั วา่ มีขอ้ มูลนยั น์ตาแต่ละสีคนละกี่สี คืนค่าเป็ น Object ดงั น้ี {brown:4,green:3,blue:5} เซพลงไฟล์
homework5-1_eyes.json
2. นบั วา่ มีขอ้ มูลเพศชายกี่คน เพศหญิงกี่คน คืนค่าเป็ น Object ดงั น้ี {male:10,female:16} เซพลงไฟล์ homework5-
1_gender.json
3. นบั วา่ มีเพื่อนท้งั หมดกี่คนและคืนค่าเป็ น Object ดงั น้ี (array ของ object ตามจ านวนคน) เซพลงไฟล์ homework5-
1_friends.json
*/

const fs = require('fs');

//อ่าน File
function readPromise_dataFile(Filename) {
    return new Promise((resolve, reject) => {
        fs.readFile(Filename, 'utf8', (err, dataFile) => {
            if (err) reject(err);
            else resolve(dataFile);
        });
    });
}
function read_dataFile(Filename, callback) {
    fs.readFile(Filename, 'utf8', (err, dataFile) => {
        if (err)
            callback(err, null);
        else
            callback(null, dataFile);
    });
}

//เขียน File
function writePromise_dataFile(Filename, dataFile) {
    return new Promise((resolve, reject) => {
        fs.writeFile(Filename, dataFile, 'utf8', (err) => {
            if (err) reject(err);
            else resolve();
        });
    });
}

//Async Promise
async function create_Data_homework5_1(typeAction) {
    try {
        const JSON_Data = await readPromise_dataFile('homework5.json');
        const employeeData = JSON.parse(JSON_Data);
        //นับจำนวนสีตา
        if (typeAction === '' || typeAction === '1') {
            let obj_employeeRecord_eyes = {};

            employeeData.forEach((currentValue_employeeRecord, index_employeeRecord, arr_employeeRecord) => {
                Object.keys(currentValue_employeeRecord).forEach((currentValue_employeeField, index_employeeField, arr_employeeField) => {
                    if (currentValue_employeeField == 'eyeColor') {
                        obj_employeeRecord_eyes[arr_employeeRecord[index_employeeRecord].eyeColor] = 0;
                    }
                });
            });

            Object.keys(obj_employeeRecord_eyes).forEach((currentValue_employeeRecord_eyes, index_employeeRecord_eyes, arr_employeeRecord_eyes) => {
                const employeeDataFilter = employeeData.filter(currentValue_employeeData => {
                    return (currentValue_employeeData.eyeColor == currentValue_employeeRecord_eyes);
                });
                obj_employeeRecord_eyes[currentValue_employeeRecord_eyes] = employeeDataFilter.length;
            });
            
            dataFile = JSON.stringify(obj_employeeRecord_eyes);
            await writePromise_dataFile('homework5-1_eyes.json', dataFile);

            if (typeAction == '1') return dataFile;
        }
        //นับจำนวนเพศ
        if (typeAction === '' || typeAction === '2') {
            let obj_employeeRecord_gender = {};

            employeeData.forEach((currentValue_employeeRecord, index_employeeRecord, arr_employeeRecord) => {
                Object.keys(currentValue_employeeRecord).forEach((currentValue_employeeField, index_employeeField, arr_employeeField) => {
                    if (currentValue_employeeField == 'gender') {
                        obj_employeeRecord_gender[arr_employeeRecord[index_employeeRecord].gender] = 0;
                    }
                });
            });

            Object.keys(obj_employeeRecord_gender).forEach((currentValue_employeeRecord_gender, index_employeeRecord_gender, arr_employeeRecord_gender) => {
                const employeeDataFilter = employeeData.filter(currentValue_employeeData => {
                    return (currentValue_employeeData.gender == currentValue_employeeRecord_gender);
                });
                obj_employeeRecord_gender[currentValue_employeeRecord_gender] = employeeDataFilter.length;
            });
            
            dataFile = JSON.stringify(obj_employeeRecord_gender);
            await writePromise_dataFile('homework5-1_gender.json', dataFile);

            if (typeAction == '2') return dataFile;
        }
        //นับจำนวนเพื่อน
        if (typeAction === '' || typeAction === '3') {
            function set_mapData_employeeDataNew(currentValue_employeeData, index_employeeData, arr_employeeData) {
                let obj_employeeRecord_friends = {};
                
                obj_employeeRecord_friends['_id'] = currentValue_employeeData._id;
                obj_employeeRecord_friends['friendCount'] = currentValue_employeeData.friends.length;

                return obj_employeeRecord_friends;
            }
            const employeeDataNew = employeeData.map(set_mapData_employeeDataNew);    
                
            dataFile = JSON.stringify(employeeDataNew);
            await writePromise_dataFile('homework5-1_friends.json', dataFile);

            if (typeAction == '3') return dataFile;
        }

        console.log('All completed! - ( Async Promise )');
    } catch (error) {
        console.error(error);
    }
}

create_Data_homework5_1('');

//Async not Promise
/* 
read_dataFile('./homework5.json', (err, dataFile) => {
    console.log('All completed! - ( Async not Promise )');
});
 */

module.exports = {
        readPromise_dataFile,
        read_dataFile,
        writePromise_dataFile,
        create_Data_homework5_1
    };