import React, { Component } from 'react';
import { Form, Icon, Input, Button, Card } from 'antd';

import './App.css';

class homework26_1 extends Component {
  render() {
    return (
      <div className="App">
        <h1> Welcome to my application </h1>
        <Card title="Sign In" style={{ width: 450, margin: 'auto', textAlign: 'left'}}>
          <Form className="login-form">
            <Form.Item>
              <div> User Name : </div>
              <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
              <div> Password : </div>
              <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="password" />
            </Form.Item>
            <Form.Item style={{ textAlign: 'center'}}>
              <Button type="primary" htmlType="submit" className="login-form-button"> Sign in </Button>
              <a className="login-form-forgot" href="./homework26_1"> Forgot password </a>
            </Form.Item>
            <hr style={{ color: 'rgba(0,0,0,.25)' }} />
            <br />
            <Form.Item>
              <div> New User : </div>
              <Button type="primary" htmlType="submit" className="login-form-button"> Sign up </Button>
            </Form.Item>
          </Form>
        </Card>
      </div>
    );
  };
};

export default homework26_1;
