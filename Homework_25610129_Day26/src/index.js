import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import HW1 from './homework26_1';
//import Todo from './todolist';


import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<HW1 />, document.getElementById('root'));
registerServiceWorker();
