"use Strict";

module.exports = {
    list,
    create,
    find,
    changeContent,
    remove,
    markComplete,
    markIncomplete
}

function createEntity (row) {
    if (row) {
        return {
            id: row.id,
            content: row.content,
            mark: row.mark || ''
        };
    };
};

async function list (db) {
    const [rows] = await db.execute(
        `SELECT
            id, content, mark
        FROM todos
        ORDER BY id`
    );
    return rows.map(createEntity);
};

async function create (db, todo) {
    let content = todo.content || '';

    if (content !== '') {
        const result = await db.execute(
            `INSERT INTO todos (
                content
            ) values (
                ?
            )
            `, [content]
        );
        return result[0].insertId;
    };
};

async function find (db, id) {
    const [rows] = await db.execute(
        `SELECT
            id, content, mark
        FROM todos
        WHERE id = ?
        `, [id]
    );
    return createEntity(rows[0]);
};

async function changeContent (db, id, content) {
    const [rows] = await db.execute(
        `UPDATE todos
        SET 
            content = ?
        WHERE id = ?
        `, [content, id]
    );
    return await find(db, id);
};

async function remove (db, id) {
    const [rows] = await db.execute(
        `DELETE FROM todos
        WHERE id = ?
        `, [id]
    );
    return await find(db, id);
};

async function markComplete (db, id) {
    const [rows] = await db.execute(
        `UPDATE todos
        SET mark = 'Complete'
        WHERE id = ?
        `, [id]
    );
    return await find(db, id);
};

async function markIncomplete (db, id) {
    const [rows] = await db.execute(
        `UPDATE todos
        SET mark = Null
        WHERE id = ?
        `, [id]
    );
    return await find(db, id);
};
