"use Strict";

const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const mysql2 = require('mysql2/promise');

const dbConfig = require('./config/database');
const pool = mysql2.createPool(dbConfig);

const makeAuthCtrl = require('./controllers/auth');
const authRepo = require('./repositorys/auth');
const authCtrl = makeAuthCtrl(pool, authRepo);

const makeUploadCtrl = require('./controllers/upload');
const uploadRepo = require('./repositorys/upload');
const uploadCtrl = makeUploadCtrl(pool, uploadRepo);

const makeUserCtrl = require('./controllers/user');
const userRepo = require('./repositorys/user');
const userCtrl = makeUserCtrl(pool, userRepo);

const maketweetCtrl = require('./controllers/tweet');
const tweetRepo = require('./repositorys/tweet');
const tweetCtrl = maketweetCtrl(pool, tweetRepo);

const makeNotificationCtrl = require('./controllers/notification');
const notificationRepo = require('./repositorys/notification');
const notificationCtrl = makeNotificationCtrl(pool, notificationRepo);

const makeMessageCtrl = require('./controllers/message');
const messageRepo = require('./repositorys/message');
const messageCtrl = makeMessageCtrl(pool, messageRepo);

const router = new Router()
/*  Auth */
    .post('/auth/signup',authCtrl.signup)
    .post('/auth/signin',authCtrl.signin)
    .get('/auth/signout',authCtrl.signout)
    .post('/auth/verify',authCtrl.verify)
/*  Upload */
    .post('/upload',uploadCtrl.upload)
/*  User */
    .patch('/user/:id',userCtrl.update)
    .put('/user/:id/follow',userCtrl.follow)
    .delete('/user/:id/follow',userCtrl.unfollow)
    .get('/user/:id/follow',userCtrl.getfollow)
    .get('/user/:id/followed',userCtrl.getfollowed)
/*  Tweet */
    .get('/tweet',tweetCtrl.list)
    .post('/tweet',tweetCtrl.create)
    .put('/tweet/:id/like',tweetCtrl.like)
    .delete('/tweet/:id/like',tweetCtrl.dislike)
    .post('/tweet/:id/retweet',tweetCtrl.retweet)
    .put('/tweet/:id/vote/:voteId',tweetCtrl.vote)
    .post('/tweet/:id/reply',tweetCtrl.reply)
/*  Notification */
    .get('/notification',notificationCtrl.list)
/*  Direct Message */
    .get('/message',messageCtrl.list)
    .get('/message/:userId',messageCtrl.get)
    .post('/message/:userId',messageCtrl.create);

const app = new Koa();
app.use(bodyParser());
app.use(requestLogger);
app.use(errorRecovery);
app.use(router.routes());
app.listen(3000);

async function requestLogger (ctx, next) {
    const start = process.hrtime();
    await next();
    const diff = process.hrtime(start);
    console.log(`${ctx.method} - ${ctx.status} - ${ctx.path}  ( ${diff[0] * 1e9 + diff[1]}ns )`);
};

async function errorRecovery (ctx, next) {
    try {
        await next();
    } catch (err) {
        console.log('Server Error: ' + err);
        ctx.status = 500;
        ctx.body = 'Error : 500';        
    };
};