"use Strict";

const _ = require('lodash');

module.exports = function (pool, repository) {
    return {
        async list (ctx) {
            ctx.body = await repository.list(pool);
        },
        async create (ctx) {
            const todo = ctx.request.body;

            if (!_.isEmpty(todo)) {
                const id = await repository.create(pool, todo) || '';
                
                if (id !== '') {
                    ctx.body = { id };    
                } else {
                    ctx.body = 'Error : 400 - Bad Request';        
                }
            } else {
                ctx.body = 'Error : 400 - Bad Request';                
            }
        },
        async get (ctx) {
            const id = Number(ctx.params.id) || 0;

            if (id <= 0) {
                ctx.status = 400;
                ctx.body = 'Error : 400 - Bad Request';                
                return;
            };

            ctx.body = await repository.find(pool, id);
        },
        async update (ctx) {
            const id = Number(ctx.params.id) || 0;
            const todo = ctx.request.body;

            if (id <= 0) {
                ctx.status = 400;
                ctx.body = 'Error : 400 - Bad Request';                
                return;
            } else if (todo.content === '') {
                ctx.status = 400;
                ctx.body = 'Error : 400 - Bad Request';     
                return;           
            };

            ctx.body = await repository.changeContent(pool, id, todo.content);
        },
        async remove (ctx) {
            const id = Number(ctx.params.id) || 0;

            if (id <= 0) {
                ctx.status = 400;
                ctx.body = 'Error : 400 - Bad Request';                
                return;
            };

            ctx.body = await repository.remove(pool, id);
        },
        async complete (ctx) {
            const id = Number(ctx.params.id) || 0;

            if (id <= 0) {
                ctx.status = 400;
                ctx.body = 'Error : 400 - Bad Request';                
                return;
            };

            ctx.body = await repository.markComplete(pool, id);
        },
        async incomplete (ctx) {
            const id = Number(ctx.params.id) || 0;

            if (id <= 0) {
                ctx.status = 400;
                ctx.body = 'Error : 400 - Bad Request';                
                return;
            };

            ctx.body = await repository.markIncomplete(pool, id);
        }
    }; 
}; 