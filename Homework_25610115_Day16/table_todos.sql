drop table todos;

create table todos (
    id int auto_increment,
    content varchar(50),
    mark varchar(10),
    created_at timestamp not null default now(),
    primary key (id)
);