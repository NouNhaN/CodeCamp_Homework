
------- จากการบ้านที่ 2 ข้อ 1 ให้หาว่าใครเป็นคนสอน และราคาเท่าไร (ห้ามแสดงคอร์สซ้ำ) โดยที่ให้ใช้คำสั่งแค่ statement เดียวเท่านั้น
------- (มีคอร์สไหนบ้างที่มีคนเรียน (ห้ามแสดงชื่อคอร์สซ้ำ))
            SELECT DISTINCT(c.name) AS course_name, c.price AS course_price
                , IF(i.name IS NULL,'-',i.name) AS instructors_name
            FROM ((enrolls AS e 
                INNER JOIN courses AS c ON c.id = e.course_id)
                LEFT JOIN instructors AS i ON i.id = c.teach_by)
            ORDER BY c.name;
/*
            +-------------------------------------+--------------+------------------+
            | course_name                         | course_price | instructors_name |
            +-------------------------------------+--------------+------------------+
            | Cooking #2                          |           90 | Gordon Ramsay    |
            | Database System Concept             |           30 | -                |
            | Dramatic Writing                    |           90 | David Mamet      |
            | Electronic Music Production         |           90 | Deadmau5         |
            | Filmmaking                          |           90 | Werner Herzog    |
            | JavaScript for Beginner             |           20 | -                |
            | Photography                         |           90 | Annie Leibovitz  |
            | Screenwriting                       |           90 | Aaron Sorkin     |
            | Shooting, Ball Handler, and Scoring |           90 | Stephen Curry    |
            +-------------------------------------+--------------+------------------+            
*/
------- จากการบ้านที่ 2 ข้อ 2 ให้หาว่าใครเป็นคนสอน และราคาเท่าไร (ห้ามแสดงคอร์สซ้ำ) โดยที่ให้ใช้คำสั่งแค่ statement เดียวเท่านั้น
------- (มีคอร์สไหนบ้างที่ไม่มีคนเรียน)
            SELECT c.name AS course_name, c.price AS course_price
                , IF(i.name IS NULL,'-',i.name) AS instructors_name
            FROM ((courses AS c
                LEFT JOIN enrolls AS e ON e.course_id = c.id)
                LEFT JOIN instructors AS i ON i.id = c.teach_by)
            WHERE e.course_id IS NULL
            ORDER BY c.name;
/*            
            +--------------------------+--------------+-----------------------+
            | course_name              | course_price | instructors_name      |
            +--------------------------+--------------+-----------------------+
            | Acting                   |           90 | Samuel L. Jackson     |
            | Building a Fashion Brand |           90 | Diane Von Furstenberg |
            | Chess                    |           90 | Garry Kasparov        |
            | Comedy                   |           90 | Steve Martin          |
            | Conservation             |           90 | Dr. Jane Goodall      |
            | Cooking                  |           90 | Wolfgang Puck         |
            | Country Music            |           90 | Reba Mcentire         |
            | Design and Architecture  |           90 | Frank Gehry           |
            | Fashion Design           |           90 | Marc Jacobs           |
            | Film Scoring             |           90 | Hans Zimmer           |
            | Jazz                     |           90 | Herbie Hancock        |
            | OWASP Top 10             |           75 | -                     |
            | Singing                  |           90 | Christina Aguilera    |
            | Tennis                   |           90 | Serena Williams       |
            | The Art of Performance   |           90 | Usher                 |
            | Writing                  |           90 | Judy Blume            |
            | Writing #2               |           90 | James Patterson       |
            | Writing for Television   |           90 | Shonda Rhimes         |
            +--------------------------+--------------+-----------------------+            
*/
------- จากการบ้านที่ 3 ข้อ 2 ให้เอามาเฉพาะคอร์สที่มีคนสอน
            SELECT c.name AS course_name, c.price AS course_price
                , IF(i.name IS NULL,'-',i.name) AS instructors_name
            FROM ((courses AS c
                LEFT JOIN enrolls AS e ON e.course_id = c.id)
                LEFT JOIN instructors AS i ON i.id = c.teach_by)
            WHERE e.course_id IS NULL AND i.name IS NOT NULL
            ORDER BY c.name;
/*
            +--------------------------+--------------+-----------------------+
            | course_name              | course_price | instructors_name      |
            +--------------------------+--------------+-----------------------+
            | Acting                   |           90 | Samuel L. Jackson     |
            | Building a Fashion Brand |           90 | Diane Von Furstenberg |
            | Chess                    |           90 | Garry Kasparov        |
            | Comedy                   |           90 | Steve Martin          |
            | Conservation             |           90 | Dr. Jane Goodall      |
            | Cooking                  |           90 | Wolfgang Puck         |
            | Country Music            |           90 | Reba Mcentire         |
            | Design and Architecture  |           90 | Frank Gehry           |
            | Fashion Design           |           90 | Marc Jacobs           |
            | Film Scoring             |           90 | Hans Zimmer           |
            | Jazz                     |           90 | Herbie Hancock        |
            | Singing                  |           90 | Christina Aguilera    |
            | Tennis                   |           90 | Serena Williams       |
            | The Art of Performance   |           90 | Usher                 |
            | Writing                  |           90 | Judy Blume            |
            | Writing #2               |           90 | James Patterson       |
            | Writing for Television   |           90 | Shonda Rhimes         |
            +--------------------------+--------------+-----------------------+
*/            