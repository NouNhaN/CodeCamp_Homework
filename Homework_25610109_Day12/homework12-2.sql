------- เพิ่ม student ลงไป 10 คน
            INSERT INTO students (id, name) VALUES
            (1, 'Student_Name_01'),
            (2, 'Student_Name_02'),
            (3, 'Student_Name_03'),
            (4, 'Student_Name_04'),
            (5, 'Student_Name_05'),
            (6, 'Student_Name_06'),
            (7, 'Student_Name_07'),
            (8, 'Student_Name_08'),
            (9, 'Student_Name_09'),
            (10, 'Student_Name_10');
/*
            +----+-----------------+---------------------+
            | id | name            | created_at          |
            +----+-----------------+---------------------+
            |  1 | Student_Name_01 | 2018-01-09 11:47:20 |
            |  2 | Student_Name_02 | 2018-01-09 11:47:20 |
            |  3 | Student_Name_03 | 2018-01-09 11:47:20 |
            |  4 | Student_Name_04 | 2018-01-09 11:47:20 |
            |  5 | Student_Name_05 | 2018-01-09 11:47:20 |
            |  6 | Student_Name_06 | 2018-01-09 11:47:20 |
            |  7 | Student_Name_07 | 2018-01-09 11:47:20 |
            |  8 | Student_Name_08 | 2018-01-09 11:47:20 |
            |  9 | Student_Name_09 | 2018-01-09 11:47:20 |
            | 10 | Student_Name_10 | 2018-01-09 11:47:20 |
            +----+-----------------+---------------------+
*/

------- ให้ student ลงเรียนแต่ละวิชาที่แตกต่างกัน (อาจจะซ้ำกันบ้าง)
            INSERT INTO enrolls (student_id, course_id) VALUES
            (1, 18),
            (2, 26),
            (3, 25),
            (4, 24),
            (5, 23),
            (6, 22),
            (7, 21),
            (8, 20),
            (9, 19),
            (10, 26);
/*
            +------------+-----------+---------------------+
            | student_id | course_id | enrolled_at         |
            +------------+-----------+---------------------+
            |          1 |        18 | 2018-01-09 11:53:40 |
            |          2 |        26 | 2018-01-09 11:53:40 |
            |          3 |        25 | 2018-01-09 11:53:40 |
            |          4 |        24 | 2018-01-09 11:53:40 |
            |          5 |        23 | 2018-01-09 11:53:40 |
            |          6 |        22 | 2018-01-09 11:53:40 |
            |          7 |        21 | 2018-01-09 11:53:40 |
            |          8 |        20 | 2018-01-09 11:53:40 |
            |          9 |        19 | 2018-01-09 11:53:40 |
            |         10 |        26 | 2018-01-09 11:53:40 |
            +------------+-----------+---------------------+
*/            

------- มีคอร์สไหนบ้างที่มีคนเรียน (ห้ามแสดงชื่อคอร์สซ้ำ)
            SELECT DISTINCT(c.name) AS course_name
            FROM (enrolls AS e 
                INNER JOIN courses AS c ON c.id = e.course_id)
            ORDER BY c.name;
/*
            +-------------------------------------+
            | course_name                         |
            +-------------------------------------+
            | Cooking #2                          |
            | Database System Concept             |
            | Dramatic Writing                    |
            | Electronic Music Production         |
            | Filmmaking                          |
            | JavaScript for Beginner             |
            | Photography                         |
            | Screenwriting                       |
            | Shooting, Ball Handler, and Scoring |
            +-------------------------------------+            
*/            
------- มีคอร์สไหนบ้างที่ไม่มีคนเรียน
            SELECT c.name AS course_name
            FROM (courses AS c
                LEFT JOIN enrolls AS e ON e.course_id = c.id)
            WHERE e.course_id IS NULL
            ORDER BY c.name;
/*
            +--------------------------+
            | course_name              |
            +--------------------------+
            | Acting                   |
            | Building a Fashion Brand |
            | Chess                    |
            | Comedy                   |
            | Conservation             |
            | Cooking                  |
            | Country Music            |
            | Design and Architecture  |
            | Fashion Design           |
            | Film Scoring             |
            | Jazz                     |
            | OWASP Top 10             |
            | Singing                  |
            | Tennis                   |
            | The Art of Performance   |
            | Writing                  |
            | Writing #2               |
            | Writing for Television   |
            +--------------------------+            
*/            