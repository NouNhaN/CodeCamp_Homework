------- จากตัวอย่าง JOIN

------- มีใครบ้างที่ไม่ได้สอนอะไรเลย
            SELECT i.name
            FROM (instructors AS i 
                LEFT JOIN courses AS c ON c.teach_by = i.id)
            WHERE  c.id IS NULL
            ORDER BY i.name;
/*
            +------------------+
            | name             |
            +------------------+
            | Alice Waters     |
            | Armin Van Buuren |
            | Bob Woofward     |
            | Helen Mirren     |
            | Martin Scorsese  |
            | Ron Howard       |
            | Thomas Keller    |
            +------------------+            
*/

------- มีคอร์สไหนบ้าง ที่ไม่มีคนสอน
            SELECT c.name
            FROM (instructors AS i 
                RIGHT JOIN courses AS c ON c.teach_by = i.id)
            WHERE  i.id IS NULL
            ORDER BY c.name;
/*
            +-------------------------+
            | name                    |
            +-------------------------+
            | Database System Concept |
            | JavaScript for Beginner |
            | OWASP Top 10            |
            +-------------------------+     
*/                   