------- จากข้อมูลของการบ้านวันที่ 11
------- หาว่าพนักงานแต่ละคน มีใครเป็น manager บ้าง
        SELECT e.emp_no, e.first_name, e.last_name
        FROM (employees AS e 
            INNER JOIN dept_manager AS dm ON dm.emp_no = e.emp_no)
        ORDER BY e.emp_no;
/*
        +--------+-------------+--------------+
        | emp_no | first_name  | last_name    |
        +--------+-------------+--------------+
        | 110022 | Margareta   | Markovitch   |
        | 110039 | Vishwani    | Minakawa     |
        | 110085 | Ebru        | Alpin        |
        | 110114 | Isamu       | Legleitner   |
        | 110183 | Shirish     | Ossenbruggen |
        | 110228 | Karsten     | Sigstam      |
        | 110303 | Krassimir   | Wegerle      |
        | 110344 | Rosine      | Cools        |
        | 110386 | Shem        | Kieras       |
        | 110420 | Oscar       | Ghazalie     |
        | 110511 | DeForest    | Hagimont     |
        | 110567 | Leon        | DasSarma     |
        | 110725 | Peternela   | Onuegbe      |
        | 110765 | Rutger      | Hofmeyr      |
        | 110800 | Sanjoy      | Quadeer      |
        | 110854 | Dung        | Pesch        |
        | 111035 | Przemyslawa | Kaelbling    |
        | 111133 | Hauke       | Zhang        |
        | 111400 | Arie        | Staelin      |
        | 111534 | Hilary      | Kambil       |
        | 111692 | Tonny       | Butterworth  |
        | 111784 | Marjo       | Giarratana   |
        | 111877 | Xiaobin     | Spinelli     |
        | 111939 | Yuchang     | Weedman      |
        +--------+-------------+--------------+
*/        
------- หาว่าพนักงานคนไหน เงินเดือนมากกว่า manager บ้าง
        SELECT e.emp_no, e.first_name, e.last_name, de.dept_no, s.salary
            , (SELECT MAX(s_manager.salary) 
                FROM ((employees AS e_manager 
	                INNER JOIN dept_manager AS d_manager ON e_manager.emp_no = d_manager.emp_no) 
	                INNER JOIN salaries AS s_manager ON s_manager.emp_no = e_manager.emp_no)
                WHERE d_manager.dept_no = de.dept_no) manager_salary
        FROM (((employees AS e 
	        INNER JOIN salaries AS s ON s.emp_no = e.emp_no)
                INNER JOIN dept_emp AS de ON de.emp_no = e.emp_no)
                INNER JOIN dept_manager AS dm on dm.dept_no = de.dept_no)
        WHERE de.dept_no IS NOT NULL AND
            s.salary > (SELECT MAX(s_manager.salary) 
                            FROM ((employees AS e_manager 
	                            INNER JOIN dept_manager AS d_manager ON e_manager.emp_no = d_manager.emp_no) 
	                            INNER JOIN salaries AS s_manager ON s_manager.emp_no = e_manager.emp_no)
                                WHERE d_manager.dept_no = de.dept_no)
        LIMIT 100;
/*
        +---------+-----------------------+
        | dept_no | MIN(s_manager.salary) |
        +---------+-----------------------+
        | d001    |                 69941 |
        | d002    |                 52070 |
        | d003    |                 40000 |
        | d004    |                 40000 |
        | d005    |                 40000 |
        | d006    |                 40055 |
        | d007    |                 70787 |
        | d008    |                 48077 |
        | d009    |                 40000 |
        +---------+-----------------------+
*/
