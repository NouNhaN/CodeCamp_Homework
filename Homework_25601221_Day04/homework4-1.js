/*
• บรรทดั แรกของไฟลก์ ารบา้ นมีดงั น้ี let arr = [1,2,3,4,5,6,7,8,9,10];
• จงคัดเลือกสมาชิกใน array ให้เหลือเพียงสมาชิกที่หาร 2 ลงตวั และนาสมาชิกเหล่าน้นั มาคูณ 1,000 ทุกตัว
• ห้ามใช้ loop for, while ในโจทยข์ อ้ น้ีเดด็ ขาด ใชไ้ ดแ้ ค่ map, reduce, filter เท่าน้นั
• สร้างไฟล์ชื่อ homework4-1.js
*/

let arr = [1,2,3,4,5,6,7,8,9,10];

//สมาชิกที่หาร 2 ลงตัว
const arr_filter = arr.filter(number => {
    return (number % 2 === 0);
});
console.log(arr_filter);

//คูณ 1000 ในสมาชิกทุกตัว
const arr_map = arr_filter.map(number => {
    return (number  * 1000);
});
console.log(arr_map);